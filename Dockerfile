FROM php:5.6-apache
LABEL maintainer="gaesi@gaesi.org"

# Install PHP extensions and PECL modules.
RUN buildDeps=" \
        default-libmysqlclient-dev \
        libbz2-dev \
        libmemcached-dev \
        libsasl2-dev \
    " \
    runtimeDeps=" \
        curl \
        git \
	wget \
	software-properties-common \
        libfreetype6-dev \
        libicu-dev \
        libjpeg-dev \
        libldap2-dev \
        libmcrypt-dev \
        libmemcachedutil2 \
        libpng-dev \
        libpq-dev \
        libxml2-dev \
	libxslt-dev \
    " \
    && apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y $buildDeps $runtimeDeps \
    && docker-php-ext-install bcmath bz2 calendar iconv intl mbstring mcrypt mysql mysqli opcache pdo_mysql pdo_pgsql pgsql soap zip json xsl xmlrpc\
    && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install gd \
    && docker-php-ext-configure ldap --with-libdir=lib/x86_64-linux-gnu/ \
    && docker-php-ext-install ldap \
    && docker-php-ext-install exif \
    && pecl install apcu-4.0.11 \
    && docker-php-ext-enable apcu \
    && apt-get purge -y --auto-remove $buildDeps \
    && rm -r /var/lib/apt/lists/*

RUN a2enmod actions alias auth_basic auth_digest authn_anon authn_dbd authn_dbm authz_core autoindex cache cgi cgid dav dbd deflate dir expires ext_filter headers include info ldap mime mime_magic \
    && a2enmod negotiation php5 proxy proxy_ajp proxy_connect proxy_html proxy_http reqtimeout rewrite setenvif status substitute suexec vhost_alias xml2enc \
    && a2enmod rewrite

# Install Composer.
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer \
    && ln -s $(composer config --global home) /root/composer
ENV PATH=$PATH:/root/composer/vendor/bin COMPOSER_ALLOW_SUPERUSER=1
RUN composer global require 'robmorgan/phinx=v0.3.7' \
    && ln -s /root/composer/vendor/robmorgan/phinx/bin/phinx /usr/bin/phinx

RUN wget --no-check-certificate https://github.com/phalcon/cphalcon/archive/phalcon-v2.0.3.tar.gz \
    && tar xzf phalcon-v2.0.3.tar.gz \
    && cd cphalcon-phalcon-v2.0.3/build \
    && ./install

COPY ./confs/* /etc/apache2/sites-enabled/
RUN chown -R www-data:www-data /etc/apache2/sites-enabled/
COPY ./src /src
RUN chown -R www-data:www-data /src

RUN rm /var/log/apache2/* \
    && touch /var/log/apache2/access.log \
    && touch /var/log/apache2/error.log \
    && chown -R www-data:www-data /var/log/apache2 \
    && service apache2 restart

ENTRYPOINT ["tail", "-f", "/var/log/apache2/error.log"]
