<?php

namespace Intelletto\Bootstrap;

class Module
{
	public function registerAutoloaders()
	{}

	public function registerServices($di)
	{
        $options = $di['config']->get('view');
        $module  = explode("\\", get_class($this))[0];
        $options['viewsDir'] = sprintf('%s/modules/%s/views/', APPLICATION_PATH, $module);
        
        $service = new Service\View($options);
        
		$di['view'] = $service->getService($di);
	}
}