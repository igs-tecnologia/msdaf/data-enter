<?php

namespace Intelletto\Bootstrap\Service;

use Phalcon\DI;

class Url extends ServiceAbstract
{
    public function getService(DI $di)
    {
        $options = $this->options;

        return function() use ($options) {
            $url =  new \Phalcon\Mvc\Url();
            $url->setBaseUri($options['baseUri']);
            
            return $url;
        };
    }
}