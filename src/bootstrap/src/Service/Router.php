<?php

namespace Intelletto\Bootstrap\Service;

use Phalcon\DI;

class Router extends ServiceAbstract
{
    public function getService(DI $di)
    {
        $options = $this->options;
        
        return function () use ($options) {
            $router = new \Phalcon\Mvc\Router();
            
            if (! isset($options['default'])) {
                $options['default'] = [
                    'module' => 'core',
                    'controller' => 'index',
                    'action' => 'index'
                ];
            }
            
            $router->add('/:module', ['module' => 1]);
            $router->add('/:module/:controller', ['module' => 1,'controller' => 2]);
            $router->add('/:module/:controller/:action/:params', [
                'module' => 1,
                'controller' => 2,
                'action' => 3,
                'params' => 4
            ]);
            
            $router->setDefaults((array) $options['default']);
            $router->removeExtraSlashes(true);
            
            return $router;
        };
    }
}