<?php

namespace Intelletto\Bootstrap\Service;
use Phalcon\Logger\Adapter\File as FileAdapter;
use Phalcon\DI;

class Logger extends ServiceAbstract
{
    public function getService(DI $di)
    {
        $options = $this->options;

        return function() use ($options) {
            return new FileAdapter($options['filename']);
        };
    }
}