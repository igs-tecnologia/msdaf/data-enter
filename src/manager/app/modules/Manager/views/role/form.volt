<style type="text/css" media="screen">
    .ui.form .fields .field:first-child{padding-left:1%;}
</style>
<div class="thirteen wide column">
    <form action="manager/role/save" method="post" class="ui form">
        <h3 class="body-title-page" data-menu=".perfis" style="margin-bottom:30px">
            <a href="manager/role" title="Voltar para a página anterior"><i class="large angle left icon"></i></a>Novo perfil
        </h3>
        <div class="ui segment form-bg">
                <div class="ui error message"></div>
                <input name="id" value="{{ data.id }}" type="hidden">
                <div class="two fields">
                <div class="field">
                    <label>Nome</label>
                    <div class="ui fluid labeled input">
                        <input id='key' name="name" value="{{ data.name }}" type="text">
                        <div class="ui corner label">
                            <i class="icon asterisk"></i>
                        </div>                            
                    </div>
                </div>
                <div class="field">
                    <label>Visível</label>
                    <div class="ui fluid selection dropdown">
                        <div class="text"></div>
                        <i class="dropdown icon"></i>
                        <input name="visible" type="hidden" value="{{ data.visible ? 't' : 'f' }}">
                        <div class="menu ui transition hidden">
                            <div class="item" data-value="t">Sim</div>
                            <div class="item" data-value="f">Não</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="ui">
            <h5 class="ui header">
                Sistemas e Recursos
            </h5>
            <div class="ui small fluid accordion">
                {% for system in systems %}
                    <div class="title">
                        <i class="dropdown icon"></i>
                        {{ system.name }}
                    </div>
                    <div class="content three fields segment">
                        {% for resource in roleResource if system.id == resource.system_id %}
                            <div class="field">
                                <div class="ui toggle checkbox">
                                    <input id="res_{{resource.resource_id}}-sys_{{resource.system_id}}" name="roleResource[][{{data.id}}|{{resource.resource_id}}|{{system.id}}]" type="checkbox" {{resource.resource_checked}}>
                                    <label for="res_{{resource.resource_id}}-sys_{{resource.system_id}}">{{resource.resource_name}}</label>
                                </div>
                            </div>
                        {% endfor %}
                    </div>
                {% endfor %}
            </div>
        </div>
        <div class="ui segment basic floated right">
            <a href="#" class="link-text">voltar</a> &nbsp;
            <button type="submit" class="ui icon button"><i class="save icon"></i> Salvar</button>
        </div>
    </form>
</div>

<script type="text/javascript">
    $(function() {
        $('.papeis').addClass('active');         
    });
    
    $('.ui.form').form({
        name: {
            identifier  : 'name',
            rules: [{
                type   : 'empty',
                prompt : 'Campo de preencimento obrigatório'
            }]
        },
        visible: {
            identifier  : 'visible',
            rules: [{
                type   : 'empty',
                prompt : 'Campo de preencimento obrigatório'
            }]
        }
    });  
</script>