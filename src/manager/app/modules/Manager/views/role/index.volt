<div class="thirteen wide column">
    <h3 class="body-title-page" data-menu=".perfis" style="margin-bottom: 30px;">
        Perfis
    </h3>
    {{ flashSession.output() }}
    <div class="ui" style="margin-bottom: 30px;">
        <a href="manager/role/form" class="ui small right floated submit button"><i class="add icon"></i> Novo</a>
        <form action="manager/role" method="get">
            <div class="ui action small input">
                <input name="term" placeholder="Procurar..." type="text" value="{{ term }}">
                <div class="ui icon small button"><i class="search icon"></i></div>
            </div>
        </form>
    </div>
    <table class="ui small sortable table segment">
        <thead>
        <tr>
            <th>Nome</th>
            <th>Visível</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        {% for row in data %}
        <tr>
            <td>{{ row.name }}</td>
            <td>{{ row.visible ? 'Sim' : 'Não' }}</td>
            <td width="120" style="text-align:right">
                <a href="manager/role/form/{{ row.id }}" class="link-icon"><i class="edit link icon"></i></a>
                <a href="manager/role/delete/{{ row.id }}" class="link-icon"><i class="delete link icon"></i></a>
            </td>
        </tr>
        {% endfor %}
        </tbody>
    </table>
</div>

<script type="text/javascript">
    $(function() {
        $('.papeis').addClass('active');
    });
</script>