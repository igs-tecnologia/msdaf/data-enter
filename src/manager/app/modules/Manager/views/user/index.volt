<div class="thirteen wide column">
    <h3 class="body-title-page" data-menu=".usuarios" style="margin-bottom: 30px;">
        Usuários
    </h3>
    {{ flashSession.output() }}
    <div class="ui" style="margin-bottom: 30px;">
        <a href="manager/user/form" class="ui small right floated submit button"><i class="add icon"></i> Novo</a>
        <form action="manager/user/index" method="get">
            <div class="ui action small input">
                <input name="term" placeholder="Procurar..." type="text" value="{{ term }}">
                <div class="ui small icon button"><i class="search icon"></i></div>
            </div>
        </form>
    </div>
    <table class="ui small sortable table segment">
        <thead>
        <tr>
            <th>Nome</th>
            <th>Login</th>
            <th>Email</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        {% for row in data %}
        <tr>
            <td>{{ row.name }}</td>
            <td>{{ row.identity }}</td>
            <td>{{ row.email }}</td>
            <td width="120" style="text-align:right">
                <a href="manager/user/form/{{ row.id }}" class="link-icon"><i class="edit link icon"></i></a>
                <a href="manager/user/delete/{{ row.id }}" class="link-icon"><i class="delete link icon"></i></a>
            </td>
        </tr>
        {% endfor %}
        </tbody>
    </table>
</div>

<script type="text/javascript">
    $(function() {
        $('.usuarios').addClass('active');
    });
</script>