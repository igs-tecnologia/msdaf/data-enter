<style>
    .filter-table .quick { margin-left: 0.5em; font-size: 0.8em; text-decoration: none; }
    .fitler-table .quick:hover { text-decoration: underline; }
    td.alt { background-color: #ffc; background-color: rgba(255, 255, 0, 0.2); }
</style>
<div class="thirteen wide column gridfind">
    <h3 class="body-title-page" data-menu=".usuarios" style="margin-bottom: 30px;">
        Usuários
    </h3>
    {{ flashSession.output() }}
    <!--<div class="ui" style="margin-bottom: 30px;">
            <div class="ui action small input">
                <input name="term" placeholder="Procurar..." type="search" value="">
                <div class="ui small icon button"><i class="search icon"></i></div>
            </div>
    </div>
    -->
    <table class="ui small sortable table segment">
        <thead>
        <tr>
            <th>Codigo consulta</th>
            <th>Consulta</th>
            <th>Categoria</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        {% for row in dataReport %}
        <tr>
            <td>{{ row.co_consulta }}</td>
            <td>{{ row.no_consulta }}</td>
            {% set arraylabel = [] %}
            <?php $arraylabel = explode(',',$row->no_label);?>
            <td>
                {% for label in arraylabel %}
                    <div class="ui small label">{{ label }}</div>
                {% endfor %}
            </td>
            <td width="120" style="text-align:right">
                <div class="edit-permission" key="{{ row.co_consulta }}"><i class="edit link icon"></i></div>
            </td>
        </tr>
        {% endfor %}
        </tbody>
    </table>
    <div class="ui one column grid">
        <div class="column center aligned">
            <div class="ui">
                <div class="ui borderless pagination menu">
                    pages
                    {% if pages >1 %}
                    {% set offlimit = (offset-limit) %}
                    {% if offlimit < 1 %}
                    {% set offlimit = 0 %}
                    {% endif %}
                    <div class="item first" offset="{{offlimit}}" limit="{{limit}}" href="#" >
                        <i class="left arrow icon"></i> Anterior
                    </div>
                    {% endif %}
                    {% set starpage = 0 %}
                    {% set endpage = 0 %}
                    <?php if($current_page==1){
                                $starpage=$current_page;
                            }else{
                                if($current_page-5<=1){
                                    $starpage = 1;
                                }else{
                                    $starpage = $current_page-5;
                                }
                            }?>
                    <?php $endpage = $current_page==$pages?$current_page:$current_page+5>=$pages?$pages:$pages-$current_page; ?>
                    {% for i in starpage..endpage %}
                    <div class="item page <?php if($i == 1){echo 'active';} ?>" offset="{{ (i-1)*limit}}" limit="{{limit}}" href="#" >{{ i }}</div>
                    {% endfor %}
                    {% if pages>1 %}
                    <div class="item next" offset="{{ (offset+limit)}}" limit="{{limit}}" href="#">
                        Próximo <i class="icon right arrow"></i>
                    </div>
                    {% endif %}
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modal-record" class="ui large modal">
    <div class="header">
        <b>Adicionar Permissão</b>
        <div style="float: right;cursor: pointer;"><i class="close icon"></i></div>
    </div>
    <div class="content">
        <form action="manager/report/save" method="post" accept-charset="utf-8" class="ui form record" enctype="multipart/form-data">
            <div id="check-form" class="ui content two fields">
            <input type="hidden" name="oldpermission" id="oldpermission" />
            </div>
        <div class="ui error message"></div>
        </form>
    </div>
    <div class="actions">
        <button class="ui button deny" type="submit">
            SALVAR
        </button>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        $('.dashboard').addClass('active');
    });
    $(document).ready(function(){
        $('.gridfind tbody tr').hide();
        $('.gridfind tbody tr:lt({{limit}})').show();
        $('.gridfind .pagination .first').css('cursor','pointer');
        $('.gridfind .pagination .next').css('cursor','pointer');
        $('.gridfind .pagination .page').css('cursor','pointer');
        $('.gridfind .pagination .page, .gridfind .pagination .first, .gridfind .pagination .next').click(function(){
            $('.pagination.menu .item').removeClass('active');
            $(this).addClass('active');
            var offset=$(this).attr('offset');
            $('.gridfind tbody tr').hide();
            $('.gridfind tbody tr:gt('+offset+'):lt({{limit}})').show();
        });
        $("table").tablesorter({
            headers: {
                3: {
                    sorter: false
                }
            }
        });
        $("table").filterTable({containerClass:'ui action input',containerTag:'div',placeholder:'Procurar...',label:'<div class="ui small icon button"><i class="search icon"></i></div>'});

        $("table .edit-permission").click(function(ev){
            var id= $(this).attr('key');
            var modal = $('#modal-record');
            modal.modal('setting', {
                offset: 400,
                detachable: false,
                onDeny    : function(){
                    var form=$('.ui.form.record',modal);
                        form.attr('action',form.attr('action')+'/'+id);
                    form.submit();
                    return false;
                }
            }).modal('show');
            $.get( "manager/report/get/"+id, function( data ) {
                $('#oldpermission',modal).val(JSON.stringify(data));
                $('.namepermission',modal).remove();
                $.each(data, function(key,rep) {
                    var checked=''
                    if(rep.co_consulta_perfil_arvore!=0){
                        checked='checked="true"';
                    }
                    $('#check-form',modal).append('<div class="field namepermission"><div class="ui toggle checkbox"><input id="co_perfil-'+key+'" name="co_perfil['+rep.co_perfil+'|'+rep.co_consulta_perfil_arvore+']" type="checkbox" '+checked+'><label for="co_perfil-'+key+'">'+rep.ds_perfil+'</label></div></div>')
                });
            },"json");
        });

    });
</script>