<div class="thirteen wide column">
    <h2 class="ui header" data-menu=".recursos">Recurso</h2>
    <div class="ui">
        <form action="manager/resource/save" method="post" class="ui form segment">
            <div class="ui error message"></div>
            <input name="id" value="{{ resource.id }}" type="hidden">
            <div class="field">
                <label>Descrição</label>
                <div class="ui left labeled input">
                    <input id='description' name="description" value="{{ resource.description }}" type="text">
                    <div class="ui corner label">
                        <i class="icon asterisk"></i>
                    </div>
                </div>
            </div>            
            <div class="two fields">
            <div class="field">
                <label>Controle</label>
                <div class="ui left labeled input">
                    <input id='controller' name="controller" value="{{ resource.controller }}" type="text">
                </div>
            </div>
            <div class="field">
                <label>Ação</label>
                <div class="ui left labeled input">
                    <input id='action' name="action" value="{{ resource.action }}" type="text">
                </div>
            </div>                
            </div>
            <div class="two fields">
            <div class="field">
                <label>Rota</label>
                <div class="ui left labeled input">
                    <input id='route' name="route" value="{{ resource.route }}" type="text">
                    <div class="ui corner label">
                        <i class="icon asterisk"></i>
                    </div>
                </div>
            </div>
            <div class="field">
                <label>Método</label>
                <div class="ui fluid selection dropdown">
                    <div class="text">Selecione</div>
                    <i class="dropdown icon"></i>
                    <input name="method" type="hidden" value="{{ resource.method }}">
                    <div class="menu ui transition hidden">
                        <div class="item" data-value="GET">GET</div>
                        <div class="item" data-value="POST">POST</div>
                        <div class="item" data-value="PUT">PUT</div>
                        <div class="item" data-value="DELETE">DELETE</div>
                    </div>
                    <div class="ui corner label">
                        <i class="icon asterisk"></i>
                    </div>
                </div>
            </div>               
            </div>
            <input type="submit" class="ui blue submit button" value="Salvar">
        </form>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        $('.recursos').addClass('active');         
    });
    
    $('.ui.form').form({
        description: {
            identifier  : 'description',
            rules: [{
                type   : 'empty',
                prompt : 'Campo de preencimento obrigatório'
            }]
        },        
        route: {
            identifier  : 'route',
            rules: [{
                type   : 'empty',
                prompt : 'Campo de preencimento obrigatório'
            }]
        },
        method: {
            identifier  : 'method',
            rules: [{
                type   : 'empty',
                prompt : 'Campo de preencimento obrigatório'
            }]
        }
    });  
</script>