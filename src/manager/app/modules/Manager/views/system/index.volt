<div class="thirteen wide column">
    <h3 class="body-title-page" data-menu=".sistemas" style="margin-bottom: 30px;">
        Sistemas
    </h3>
    {{ flashSession.output() }}

    <div class="ui" style="margin-bottom: 30px;">
        <a href="manager/system/form" class="ui small right floated submit button"><i class="add icon"></i> Novo</a>
        <form action="manager/system" method="get">
            <div class="ui action small input">
                <input name="term" placeholder="Procurar..." type="text" value="{{ term }}">
                <div class="ui small icon button"><i class="search icon"></i></div>
            </div>
        </form>
    </div>
    <table class="ui table small segment">
        <thead>
            <tr>
                <th>Nome</th>
                <th>Visível</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            {% for system in data %}
                <tr>
                    <td>{{ system.name }}</td>
                    <td>{{ system.visible ? 'Sim' : 'Não' }}</td>
                    <td width="120" style="text-align:right">
                        <a href="manager/system/form/{{ system.id }}" class="link-icon"><i class="edit link icon"></i></a>
                    </td>
                </tr>
            {% endfor %}
        </tbody>
    </table>
</div>

<script type="text/javascript">
    $(function() {
        $('.sistemas').addClass('active');
    });
</script>