<div class="thirteen wide column">
    <h3 class="body-title-page" data-menu=".sistemas" style="margin-bottom:30px">
        <a href="#" title="Voltar para a página anterior"><i class="large angle left icon"></i></a>Novo sistema
    </h3>
    <div class="ui segment form-bg">
        <form action="manager/system/save" method="post" class="ui form">
            <div class="ui error message"></div>
            <input name="id" value="{{ system.id }}" type="hidden">
            <div class="field">
                <label>Nome</label>
                <div class="ui fluid labeled input">
                    <input id='name' name="name" value="{{ system.name }}" type="text">
                    <div class="ui corner label">
                        <i class="icon asterisk"></i>
                    </div>                            
                </div>
            </div>
            <div class="field">
                <label>Url</label>
                <div class="ui fluid labeled input">
                    <input id='url' name="return_url" value="{{ system.return_url }}" type="text">
                    <div class="ui corner label">
                        <i class="icon asterisk"></i>
                    </div>                            
                </div>
            </div>
            <div class="two fields">
            <div class="field">
                <label>Chave</label>
                <div class="ui fluid labeled input">
                    <input id='key' name="key" value="{{ system.key }}" type="text">
                    <div class="ui corner label">
                        <i class="icon asterisk"></i>
                    </div>                            
                </div>
            </div>
            <div class="field">
                <label>Senha</label>
                <div class="ui fluid labeled input">
                    <input id='secret' name="secret" value="{{ system.secret }}" type="text">
                    <div class="ui corner label">
                        <i class="icon asterisk"></i>
                    </div>                            
                </div>
            </div>                    
            </div>
            <div class="four fields">
            <div class="field">
                <label>Visível</label>
                <div class="ui fluid selection dropdown">
                    <div class="text"></div>
                    <i class="dropdown icon"></i>
                    <input name="visible" type="hidden" value="{{ system.visible ? 't' : 'f' }}">
                    <div class="menu ui transition hidden">
                        <div class="item" data-value="t">Sim</div>
                        <div class="item" data-value="f">Não</div>
                    </div>
                </div>
            </div>
            </div>

        </form>
    </div>
    <div class="ui segment basic floated right" style="margin:0;padding:0">
        <a href="#" class="link-text">voltar</a> &nbsp;
        <!--input type="submit" class="ui submit button" value="Salvar"-->
        <button type="submit" class="ui icon button"><i class="save icon"></i> Salvar</button>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        $('.sistemas').addClass('active');         
    });
    
    $('.ui.form').form({
        name: {
            identifier  : 'name',
            rules: [{
                type   : 'empty',
                prompt : 'Campo de preencimento obrigatório'
            }]
        },
        return_url: {
            identifier  : 'return_url',
            rules: [{
                type   : 'empty',
                prompt : 'Campo de preencimento obrigatório'
            }]
        },
        key: {
            identifier  : 'key',
            rules: [{
                type   : 'empty',
                prompt : 'Campo de preencimento obrigatório'
            }]
        }, 
        secret: {
            identifier  : 'secret',
            rules: [{
                type   : 'empty',
                prompt : 'Campo de preencimento obrigatório'
            }]
        }, 
        visible: {
            identifier  : 'visible',
            rules: [{
                type   : 'empty',
                prompt : 'Campo de preencimento obrigatório'
            }]
        }
    });  
</script>