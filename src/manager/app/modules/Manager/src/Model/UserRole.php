<?php

namespace Manager\Model;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;

class UserRole extends Model 
{
    public $role_id;
    public $system_id;
    public $user_id;

    public function initialize() 
    {
        $this->setSchema("public");
        $this->setSource("user_role");
    }
    
    public static function findAllChecked($userId)
    {
        if ($userId) {
            $sql = "SELECT s.id system_id, r.id role_id, s.name system_name, r.name role_name,
                    (SELECT 'checked' FROM user_role ur WHERE u.id = ur.user_id AND s.id = ur.system_id AND r.id = ur.role_id) role_checked
                    FROM \"user\" u, system s, role r
                    WHERE u.id = ".$userId." AND s.visible = 'true' AND r.visible = 'true' 
                    ORDER BY s.name, r.name";
        } else {
            $sql = "SELECT s.id system_id, r.id role_id, s.name system_name, r.name role_name, '' role_checked
                    FROM system s, role r
                    WHERE s.visible = 'true' AND r.visible = 'true'
                    ORDER BY s.name, r.name";            
        } 
        
        $model = new UserRole();
         
        return new Resultset(null, $model, $model->getReadConnection()->query($sql)); 
    }
    
    public static function visibleRolesByUser($userId)
    {
        $sql = "SELECT ur.* FROM user_role ur, role r
                WHERE ur.user_id = ? 
                AND ur.role_id = r.id 
                AND r.visible = 'true'";
        
        $model  = new self;
        $conn   = $model->getReadConnection();
        $result = $conn->query($sql, [$userId]);
        
        return new Resultset(null, $model,$result); 
    }
}