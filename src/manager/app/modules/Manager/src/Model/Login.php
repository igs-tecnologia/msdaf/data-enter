<?php

namespace Manager\Model;

use Phalcon\Mvc\Model;
use Phalcon\Db\RawValue;

class Login extends Model {

    public $id;
    public $user_id;
    public $ip;
    public $user_agent;
    public $created;

    public function initialize() {
        $this->setSchema("public");
        $this->setSource("login");
        
        $this->belongsTo(
            'user_id', 'Manager\Model\User', 'id',
            ['foreignKey' => true]
        );
    }

    public function beforeValidationOnCreate()
    {
        $this->created = new RawValue('default');
    }    
}
