<?php

namespace Manager\Model;

use Phalcon\Mvc\Model;
use Phalcon\Db\RawValue;

class LoginFail extends Model {

    public $id;
    public $user_id;
    public $ip;
    public $user_agent;
    public $attempt;
    public $created;

    public function initialize() {
        $this->setSchema("public");
        $this->setSource("login_fail");
    }

    public function beforeValidationOnCreate()
    {
        $this->created = new RawValue('default');
    }    
}
