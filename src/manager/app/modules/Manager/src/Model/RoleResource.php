<?php

namespace Manager\Model;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;

class RoleResource extends Model 
{
    public $resource_id;
    public $role_id;

    public function initialize() 
    {
        $this->setSchema("public");
        $this->setSource("role_resource");

        $this->belongsTo(
            'resource_id', 
            'Manager\Model\Resource',
            'id', 
            array('alias' => 'resource')
        );
                
        $this->belongsTo(
            'role_id', 
            'Manager\Model\Role', 
            'id', 
            array('alias' => 'role')
        );
    }
    
    public static function findAllChecked($role = null)
    {
        if (isset($role)) {
            $sql = "SELECT r.id resource_id, system_id, s.name system_name, r.description resource_name, 
                    (SELECT 'checked' FROM role_resource rr WHERE s.id = r.system_id AND r.id = rr.resource_id AND rl.id = rr.role_id) resource_checked
                    FROM resource r, system s, role rl
                    WHERE  rl.id = $role AND r.system_id = s.id 
                    ORDER BY r.description";
        } else {
            $sql = "SELECT r.id resource_id, system_id, s.name system_name, r.description resource_name, '' resource_checked
                    FROM resource r,system s
                    WHERE r.system_id = s.id 
                    ORDER BY r.description";          
        } 
        
        $model = new RoleResource();
         
        return new Resultset(null, $model, $model->getReadConnection()->query($sql));         
    }    
}