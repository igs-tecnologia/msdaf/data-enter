<?php

namespace Manager\Model;

use Phalcon\Mvc\Model;

class Resource extends Model {

    public $id;
    public $description;
    public $controller;
    public $action;
    public $route;
    public $method;

    public function initialize() {
        $this->setSchema("public");
        $this->setSource("resource");

        $this->belongsTo(
                'system_id', 'Manager\Model\System', 'id', array('alias' => 'system')
        );
    }

}
