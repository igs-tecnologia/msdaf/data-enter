<?php

namespace Manager\Model;

use Phalcon\Mvc\Model;

class Role extends Model 
{
    public $id;
    public $name;

    public function initialize() 
    {
        $this->setSchema("public");
        $this->setSource("role");
        
        $this->hasMany('id', "Manager\Model\UserRole", "role_id");
    }
}