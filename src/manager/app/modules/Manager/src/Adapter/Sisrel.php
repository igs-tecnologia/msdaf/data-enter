<?php

namespace Manager\Adapter;

use Phalcon\DI\FactoryDefault;
use Manager\Service\Sisrel as Service;

class Sisrel implements AdapterInterface
{
    protected $reference;
    protected $filter;
    protected $dynamic;
    protected $di;
    
    public function __construct(array $struct)
    {
        $this->reference = $struct['reference'];
        $this->dynamic = isset($struct['dynamic']) ? $struct['dynamic'] : false;
        $this->filter  = isset($struct['filter']) ? $struct['filter'] : false;
    }
    
    public function setDI(FactoryDefault $di)
    {
        $this->di = $di;
    }
    
    public function getOptions()
    {
        $data = [];
        
        $parts = explode('.', $this->reference);
        $query = [
            'co_consulta' => $parts[0], 
            'field' => $parts[1]
        ];
               
        $rows = (new Service)->getColValues($this->di['config']->sisrel->url, $query);
        
        if (! count($rows)) {
            return $data;
        }
        
        foreach ($rows as $row) {
            if ($row['valor'] != null && $row['valor'] != '') {
                $data[$row['valor']] = $row['valor'];
            }
        }
        
        return $data;
    }
}