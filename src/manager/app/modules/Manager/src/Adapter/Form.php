<?php

namespace Manager\Adapter;

use Phalcon\DI\FactoryDefault;

class Form implements AdapterInterface
{
    protected $field;
    protected $filter;
    protected $dynamic;
    protected $di;
    
    public function __construct(array $struct)
    {
        $this->field   = $struct['field'];
        $this->dynamic = isset($struct['dynamic']) ? $struct['dynamic'] : false;
        $this->filter  = isset($struct['filter']) ? $struct['filter'] : false;
    }
    
    public function setDI(FactoryDefault $di)
    {
        $this->di = $di;
    }
    
    public function getOptions()
    {
        $data = [];
        
        list($formId, $field) = explode('.', $this->field);

        $response = $this->di['dataService']->setOrder($field)->setLimit(10000)->all($formId);
        $rows     = (array) $response->getData()['data'];

        foreach ($rows as $row) {
            $value = json_decode($row['value']);
            
            if (isset($value->$field)) {
                $data[$row['id']] = $row['id'].':'.$value->$field;
            }
        } 

        return $data;
    }
}