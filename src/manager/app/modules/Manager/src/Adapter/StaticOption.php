<?php

namespace Manager\Adapter;

class StaticOption implements AdapterInterface
{
    protected $options;
    
    public function __construct(array $struct)
    {
        $this->options = $struct['options'];
    }
    
    public function getOptions()
    {
        $options = explode(",", $this->options);
        $data = [];
        
        foreach ($options as $value) {
            $data[$value] = $value;
        }
        
        return $data;
    }
}