<?php

namespace Manager\Adapter;

interface AdapterInterface
{
    public function __construct(array $struct);
    public function getOptions();
}