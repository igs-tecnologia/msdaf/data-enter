<?php

namespace Manager\Controller;

use Manager\Model\Role;
use Manager\Model\System;
use Manager\Model\RoleResource;
use Manager\Model\UserRole;

class RoleController extends \Intelletto\Webservice\Mvc\Controller
{
    public function indexAction() 
    {
        $term = trim($this->request->get('term'));
        $where = ['order' => 'name'];

        if ($term) {
            $where = [
                'conditions' => "name LIKE ?0",
                'order' => 'name',
                "bind" => ["%$term%"]
            ];
        }

        $this->view->term = $term;
        $this->view->data = Role::find($where);
    }

    public function formAction($id = null) 
    {
        $role = new Role();
        $role->visible = "t";
        $systems = System::find(["order" => "name"]);
        $roleResource = RoleResource::findAllChecked($id);
        
        if ($id) {
            $role = Role::findFirst($id);
        }
          
        $this->view->data = $role;
        $this->view->systems = $systems;
        $this->view->roleResource = $roleResource;
    }

    public function saveAction() {
        $role = new Role();
        $roleData = $this->request->getPost();
        $roleResource = $roleData['roleResource'];
        
        unset($roleData['roleResource']);
        
        if ($roleData['id']) {
            RoleResource::findByRoleId($roleData['id'])->delete();
        }        
        
        if (!$role->save($roleData)) {
            $this->flashSession->error($role->getMessages());
            $this->response->redirect('manager/role/form/' . $role['id']);
            return false;
        }

        foreach ($roleResource as $key => $roles) {
            $roles = array_flip($roles);
            foreach ($roles as $key => $value) {
                $rrData = explode("|", $value);
                $rrDB = new RoleResource();
                $rrDB->role_id = $role->id;
                $rrDB->resource_id = $rrData[1];
                $rrDB->system_id = $rrData[2];
                $rrDB->save();
            } 
        }
        
        $this->flashSession->success('Cadastro realizado com sucesso!');
        $this->response->redirect('manager/role');        
        
    }

    public function deleteAction($id = null) {
        
        if($id) {
            UserRole::findByRoleId($id)->delete();
            Role::findFirstById($id)->delete();
        }
        $this->flashSession->success('Exclusão realizada com sucesso!');
        $this->response->redirect('manager/role');
    }



}
