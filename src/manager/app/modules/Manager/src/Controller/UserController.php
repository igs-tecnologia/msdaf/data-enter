<?php

namespace Manager\Controller;

use Manager\Model\User;
use Manager\Model\System;
use Manager\Model\UserRole;
use Manager\Model\Property;
use Manager\Model\Login;
use Manager\Model\LoginToken;
use Manager\Model\LoginFail;
use Manager\Model\UserLastRole;

class UserController extends \Intelletto\Webservice\Mvc\Controller
{
    public function indexAction() 
    {
        $term = trim($this->request->get('term'));
        $where = null;

        if ($term) {
            $where = [
                'conditions' => "name ILIKE ?0 OR identity ILIKE ?0 OR email ILIKE ?0",
                "bind" => ["%$term%"]
            ];
        }

        $this->view->term = $term;
        $this->view->data = User::find($where);
    }

    public function formAction($id = null) 
    {
        $loginFail = new \stdClass();
        $loginFail->attempt = 0;
        $user         = new User;
        $systems      = System::find(["visible = true", "order" => "name"]);
        $properties   = (array) json_decode(Property::findFirst(1)->value);
        $userRoles    = UserRole::findAllChecked($id);
        $userProperty = [];
        if ($id) {
            $user = User::findFirstById($id);
            $loginFail = LoginFail::findFirst(array(
                "conditions" => 'user_id = '.$id.'',
                "order" => 'id DESC',
            ));
            $userProperty = $user->property!='null'?json_decode($user->property,1):array();
        }
        $this->view->row            = $user;
        $this->view->attempt        = $loginFail->attempt>=3?true:false;
        $this->view->systems        = $systems;
        $this->view->userRoles      = $userRoles;
        $this->view->properties     = $this->getForms($properties);
        $this->view->userProperties = $userProperty;
        $this->view->di             = $this->getDI();
    }

    public function saveAction() 
    {
        $user      = new User;
        $userData  = $this->request->getPost();

        $userRoles = isset($userData['userRoles'])?$userData['userRoles']:array();
        $userName = User::findFirstByIdentity($userData['identity']);
        $userEmail = User::findFirstByEmail($userData['email']);

        if(!isset($userData['id'])){
            if($userName || $userEmail){
                $this->flashSession->error('Usuário ou E-mail já cadastrados.');
                return $this->response->redirect(sprintf('manager/user/form/'));
            }
        }

        foreach($userData['properties'] as $keyAdapter=>$propertyAdapter){
            foreach($propertyAdapter as $keyProperty=>$property){
                if(!$property){
                    unset($userData['properties'][$keyAdapter][$keyProperty]);
                }
            }
            if(empty($userData['properties'][$keyAdapter])){
                unset($userData['properties'][$keyAdapter]);
            }
        }

        if(empty($userData['properties'])){
            $userData['properties']=null;
        }
        $userData['property'] = json_encode($userData['properties']);
        unset($userData['userRoles']);

        if ($userData['id']) {
            $user = User::findFirstById($userData['id']);
            UserRole::visibleRolesByUser($userData['id'])->delete();
        } else {
            $secret = substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"), 0, 10);
            $userData['secret'] = password_hash($secret, CRYPT_BLOWFISH);
            $this->sendMailNewUser($userData,$secret);
        }

        if (! $user->save($userData)) {
            $this->flashSession->error($user->getMessages());
            return $this->response->redirect('manager/user/form/' . $userData['id']);
        }

        foreach ($userRoles as $key => $roles) {
            $roles = array_flip($roles);
            foreach ($roles as $key => $value) {
                $roleData = explode("|", $value);
                $roleDB = new UserRole();
                $roleDB->role_id = $roleData[0];
                $roleDB->system_id = $roleData[1];
                $roleDB->user_id = $user->id;
                $roleDB->save();
            } 
        }
        
        $this->flashSession->success('Cadastro realizado com sucesso!');
        $this->response->redirect('manager/user/');
    }

    public function deleteAction($id = null) {
        
        if($id) {
            LoginFail::findByUserId($id)->delete();
            LoginToken::findByUserId($id)->delete();
            Login::findByUserId($id)->delete();
            UserLastRole::findByUserId($id)->delete();
            UserRole::findByUserId($id)->delete();
            User::findFirstById($id)->delete();
        }
        $this->flashSession->success('Exclusão realizada com sucesso!');
        $this->response->redirect('manager/user/');
    }

    protected function getForms($properties)
    {
        $data = [];
        $forms = [];
        
        foreach ($properties as $value) {
            list($formId, $fieldName) = explode('.', $value);
            
            if (isset($forms[$formId])) {
                $form = $forms[$formId];
            } else {
                $response = $this->form->get($formId);
                $form = $response->getData()['form'];
            }
            
            $struct = array_filter((array) json_decode($form['struct']), function($value) use ($fieldName) {
                return $value->nativeName == $fieldName;
            });
            
            $forms[$formId] = $form;
            $data[] = [
                'value' => $value,
                'formId' => $formId,
                'field' => $fieldName,
                'struct' => (array) current($struct)
            ];
        }
        
        return $data;
    }
    
    public function unlockAction($id) 
    {
        $fails = LoginFail::findByUserId($id);
        $user = User::findFirstById($id)->toArray();

        foreach ($fails as $fail) {
            $fail->update(['attempt' => 0]);
        }

        $this->sendMailUnlock($user);
        $this->flashSession->success('Desbloqueio realizado com sucesso!');
        $this->response->redirect('manager/user');
    }
    
    protected function sendMailNewUser($user,$secret)
    {
        // @todo Verificar possibilidade de utilizar o partials via template do phalcon

        $identity = $user['identity'];

        $emailBody = "Informações de Acesso a plataforma da sala de situação.<br>";
        $emailBody.= "Endereço: <a href='".$this->config->idp->url."'>".$this->config->idp->url."</a><br><br>";
        $emailBody.= "Login: $identity<br>";
        $emailBody.= "Senha: <b style='color: red;'>$secret</b><br><br><br>";
        $return=$this->mailService->send($user['email'],'Novo usuário', $emailBody);

    }

    protected function sendMailUnlock($user)
    {
        // @todo Verificar possibilidade de utilizar o partials via template do phalcon
        $identity = $user['identity'];

        $emailBody = "Seu login estava bloqueado e foi desbloqueado pelo administrador do sistema .<br>";
        $emailBody.= "Acesse o endereço: <a href='".$this->config->idp->url."'>".$this->config->idp->url."</a><br><br>";
        $emailBody.= "Login: $identity<br>";
        $return=$this->mailService->send($user['email'],'Usuário Desbloqueado', $emailBody);

    }

}