<?php

namespace Manager\Controller;


class ReportController extends \Intelletto\Webservice\Mvc\Controller
{
    public function indexAction() 
    {
        $opts = array(
            'http'=>array(
                'method'=>"GET",
                'header'=>"Accept-language: en\r\n" .
                    "Cookie: http_x_forwarded_host=".$this->config->sisrel->http_x_forwarded_host_report."; perfil_cookie=".base64_encode($this->config->sisrel->perfil_cookie_report).""
            )
        );
        $context = stream_context_create($opts);
        $json = file_get_contents($this->request->getScheme().'://'.$this->request->getServer('SERVER_NAME').'/'.$this->config->sisrel->url.'?module=consulta&controller=arvore-consulta&action=list',false,$context);
        $this->view->dataReport = json_decode($json)->consulta;
        $total=array();
        $total['value'] = count($this->view->dataReport);
        $total['name'] = 'TOTAL DE CONSULTAS';
        $this->view->total = $total;
        $this->view->offset  = 0;
        $this->view->limit   = 30;
        $this->view->pages   = ceil($total['value']/$this->view->limit);
        $this->view->current_page   = ceil($this->view->offset/$this->view->limit)+1;
    }

    public function getAction($id)
    {
        $data = array ('co_consulta' => $id);
        $data = http_build_query($data);
        $context_options = array (
            'http' => array (
                'method' => 'POST',
                'header'=> "Content-type: application/x-www-form-urlencoded\r\n" .
                    "Cookie: http_x_forwarded_host=".$this->config->sisrel->http_x_forwarded_host_report."; perfil_cookie=".base64_encode($this->config->sisrel->perfil_cookie_report).""
                    . "Content-Length: " . strlen($data) . "\r\n",
                'content' => $data
            )
        );

        $context = stream_context_create($context_options);

        $json = file_get_contents($this->request->getScheme().'://'.$this->request->getServer('SERVER_NAME').'/'.$this->config->sisrel->url.'?module=perfil&controller=perfil&action=get-list-share',false,$context);
        echo $json;
        exit();
    }
    public function saveAction($id)
    {
        $jsonOld=json_decode($this->request->getPost('oldpermission'));
        $arrayAllow=array();
        $arrayAllowKey=array();
        $arrayDenny=array();
        foreach($this->request->getPost('co_perfil') as $key=>$permission){
            $explodeKey=explode('|',$key);
            if($explodeKey[1]!=0){
                $arrayAllow[]=['co_perfil'=>$explodeKey[0],'co_consulta_perfil_arvore'=>$explodeKey[1]];
            }else{
                $arrayAllow[]=['co_perfil'=>$explodeKey[0]];
            }
            $arrayAllowKey[]=$explodeKey[0];
        }
        foreach($jsonOld as $keyOld=>$permissionOld){
            if(!in_array($permissionOld->co_perfil,$arrayAllowKey)){
                if($permissionOld->co_consulta_perfil_arvore!=0){
                    $arrayDenny[]=['co_perfil'=>$permissionOld->co_perfil,'co_consulta_perfil_arvore'=>$permissionOld->co_consulta_perfil_arvore];
                }else{
                    $arrayDenny[]=['co_perfil'=>$permissionOld->co_perfil];
                }
            }
        }
        $data = array ('co_consulta' => $id,'array_perfil'=>json_encode($arrayAllow),'array_perfilnotchecked'=>json_encode($arrayDenny));
        $data = http_build_query($data);
        $context_options = array (
            'http' => array (
                'method' => 'POST',
                'header'=> "Content-type: application/x-www-form-urlencoded\r\n" .
                    "Cookie: http_x_forwarded_host=".$this->config->sisrel->http_x_forwarded_host_report."; perfil_cookie=".base64_encode($this->config->sisrel->perfil_cookie_report).""
                    . "Content-Length: " . strlen($data) . "\r\n",
                'content' => $data
            )
        );
        $context = stream_context_create($context_options);
        $json = file_get_contents($this->request->getScheme().'://'.$this->request->getServer('SERVER_NAME').'/'.$this->config->sisrel->url.'?module=consulta&controller=consulta&action=share',false,$context);

        $this->flashSession->success('Realizado com sucesso!');
        return $this->response->redirect('manager/report/index');


    }

    //get-list-share
}