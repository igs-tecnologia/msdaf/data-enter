<?php

namespace Manager\Controller;

use Manager\Model\Resource;

class ResourceController extends \Intelletto\Webservice\Mvc\Controller
{

    public function indexAction() {
        $term = trim($this->request->get('term'));
        $where = ['order' => 'description'];

        if ($term) {
            $where = [
                'conditions' => "description LIKE ?0 OR route LIKE ?0",
                'order' => 'description',
                "bind" => ["%$term%"]
            ];
        }

        $this->view->term = $term;
        $this->view->data = Resource::find($where);
    }

    public function formAction($id = null) {
        $resource = new Resource();
        
        if ($id) {
            $resource = Resource::findFirst($id);
        }
        
        $this->view->resource = $resource;
    }

    public function saveAction() {
        $resource = new Resource();
        $resourceData = $this->request->getPost();
        
        if (!$resource->save($resourceData)) {
            $this->flashSession->error($resource->getMessages());
            $this->response->redirect('manager/resource/form/' . $resource['id']);
            return false;
        }

        $this->flashSession->success('Cadastro realizado com sucesso!');
        $this->response->redirect('manager/resource');        
        
    }

    public function deleteAction($id = null) {
        
        if($id) {
            Resource::findFirstById($id)->delete();
        }
        $this->flashSession->success('Exclusão realizada com sucesso!');
        $this->response->redirect('manager/resource');
    }



}
