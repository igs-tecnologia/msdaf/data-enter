<?php

namespace Manager\Controller;

use Manager\Model\Property;

class PropertyController extends \Intelletto\Webservice\Mvc\Controller
{
    public function indexAction() 
    {
        $response = $this->form->all();

        $this->view->fields = !Property::findFirst(1)?array():json_decode(Property::findFirst(1)->value);

        $this->view->data = (array) $response->getData()['form'];
    }

    public function saveAction() 
    {
        $property = new Property();
        $data = [
            'id' => 1, 
            'key' => 'fields',
            'value' => json_encode(isset($_POST['field'])?$_POST['field']:array())
        ];

        if (! $property->save($data)) {
            $this->flashSession->error(current($property->getMessages()));
            return $this->response->redirect('manager/property/');
        }

        $this->flashSession->success('Realizado com sucesso!');
        return $this->response->redirect('manager/property/');
    }
}