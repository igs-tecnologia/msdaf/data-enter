<?php

namespace Manager\Controller;

use Manager\Model\System;
use Manager\Model\RoleResource;
use Manager\Model\UserRole;

class SystemController extends \Intelletto\Webservice\Mvc\Controller
{
    public function indexAction() 
    {
        $term = trim($this->request->get('term'));
        $where = ['order' => 'name'];

        if ($term) {
            $where = [
                'conditions' => "name LIKE ?0",
                'order' => 'name',
                "bind" => ["%$term%"]
            ];
        }

        $this->view->term = $term;
        $this->view->data = System::find($where);
    }

    public function formAction($id = null) 
    {
        $system = new System();
        $factory = new Factory();
        $generator = $factory->newKeyGenerator();
        $system->visible = "t";
        $system->key = $generator->generateKey();
        $system->secret = $generator->generateSecret();
        
        if ($id) {
            $system = System::findFirst($id);
        }
          
        $this->view->system = $system;
    }

    public function saveAction() 
    {
        $system = new System();
        $systemData = $this->request->getPost();
        
        if (!$system->save($systemData)) {
            $this->flashSession->error($system->getMessages());
            $this->response->redirect('manager/system/form/' . $system['id']);
            return false;
        }

        $this->flashSession->success('Cadastro realizado com sucesso!');
        $this->response->redirect('manager/system');                
    }

}