<?php

namespace Manager\Controller;


class IndexController extends \Intelletto\Webservice\Mvc\Controller
{
    public function indexAction() 
    {
        return $this->response->redirect('manager/system');
    }
}