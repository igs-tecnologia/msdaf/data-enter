
$(function() {

    var modal = $('#poi-modal');
    $('#map-column').click(function() {
        modal.modal('show');
    });

    $('.tabular.menu .item').tab();

    // map
    var $map = $('#map-column');
    if ($map && $map.length) {
        var resizeMap = function() {
            var totalHeight = $(window).height();
            var fixedHeight = 0;
            $('.ui.fixed.menu').each(function() {
                fixedHeight += $(this).height();
            });
            $map.css('height', totalHeight - fixedHeight);
        };
        resizeMap();
        $(window).on('resize', resizeMap);
    }

    $('.ui.accordion')
      .accordion()
    ;

    $('.delete').click(function(){
        var url = $(this).parent().attr('href');
        $("#txt-conform").html("Tem certeza que deseja excluir este registro?");

        $('.modal-delete')
            .modal('setting', {
                onApprove : function() {
                    window.location = url;
                }
            })
            .modal('show')
        ;
        return false;
    });
});
