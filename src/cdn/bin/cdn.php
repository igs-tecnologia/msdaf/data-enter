<?php
/**
 * Created by PhpStorm.
 * User: mauriciorocha
 * Date: 03/07/15
 * Time: 10:30
 */


use Phalcon\Config\Adapter\Ini,
    Phalcon\DI\FactoryDefault,
    Phalcon\Assets,
    Phalcon\Assets\FilterInterface,
    Phalcon\Assets\Filters\None,
    Phalcon\Assets\Filters\Cssmin,
    Phalcon\Assets\Manager,
    Phalcon\Assets\Filters\Jsmin;

$di = new FactoryDefault();

$dir = 'public/themes/';
$themes = scandir($dir);
unset($themes[0]);
unset($themes[1]);

echo "\n-------------------------------\n";
echo "| Iniciando compilação do CDN |\n";
echo "-------------------------------\n\n";

foreach($themes as $theme) {
    $config = new Ini("public/themes/{$theme}/app.ini");
    $di['config'] = $config;
    $pathTheme = "public/themes/{$theme}";

    if(!file_exists ("public/themes/{$theme}/tmp")) {
        mkdir("public/themes/{$theme}/tmp", 0777);
    }

    $options=$di->get('config');

    printf("Compilando o tema: %s \n", $theme);
    try
    {
        $temp = tempnam('/tmp', 'cdn-');
        file_put_contents($temp,file_get_contents("public/themes/{$theme}/app.ini"));
    }catch (Exception $e){
        var_dump($e);
    }

    $config = new \Phalcon\Config\Adapter\Ini($temp);
    unlink($temp);
    $assets = new \Phalcon\Assets\Manager();

    echo "\n-------\n";
    echo "| CSS |\n";
    echo "-------\n\n";

    foreach($config->css->file as $css){
        printf("Adicionando biblioteca de CSS: %s \n", $css);
        $assets->collection('cssHeader')->addCss('public'.$css,false);
    }
    echo "\n";
    foreach($config->css->theme as $csstheme){

        printf("Adicionando o Tema da Aplicação: %s \n", $csstheme);
        $assets->collection('cssHeader')->addCss('public'.$csstheme,false);
    }

    printf("Compilando o arquivo finalRemote.css \n");
    $assets->collection('cssHeader')->setTargetPath($pathTheme.'/tmp/finalRemote.css');
    $assets->collection('cssHeader')->setTargetUri($pathTheme.'/tmp/finalRemote.css');
    $assets->collection('cssHeader')->join(true);
    $assets->collection('cssHeader')->addFilter(new Cssmin());

    echo $assets->outputCss('cssHeader');

    echo "\n--------------\n";
    echo "| JAVASCRIPT |\n";
    echo "--------------\n\n";

    foreach($config->js->file as $js){
        printf("Adicionando arquivo JS: %s \n", $js);
        $assets->collection('jsHeader')->addJs('public'.$js,false);
    }
    printf("Compilando o arquivo finalRemote.js \n");
    $assets->collection('jsHeader')->setTargetPath($pathTheme.'/tmp/finalRemote.js');
    $assets->collection('jsHeader')->setTargetUri($pathTheme.'/tmp/finalRemote.js');
    $assets->collection('jsHeader')->join(true);
    $assets->collection('jsHeader')->addFilter(new Jsmin());

    echo $assets->outputJs('jsHeader');

}
echo "\n\n-----------------\n";
echo "| Fim. Sucesso! |\n";
echo "-----------------\n\n";