$(function() {
    $('.ui.accordion').accordion();
});

function inRange(number, min, max) {
    if (!isNaN(number) && (number >= min) && (number <= max)) {
        return true;
    }
    
    return false;
}

$.fn.form.settings.rules.date = function(value) {
    if (!value) {
        return true;
    }
    
    if (moment(value, 'DD/MM/YYYY').isValid() && value != '31/12/1969') {
        return true;
    }
    
    return false;
};

$.fn.form.settings.rules.coordinates = function(value) {
    if (!value) {
        return true;
    }
    
    var parts = value.split(',');
    
    if (parts.length != 2) {
        return false;
    }
    
    if (inRange(parseFloat(parts[0]), -90, 90) && inRange(parseFloat(parts[1]), -180, 180)) {
        return true;
    }
    
    return false;
};
