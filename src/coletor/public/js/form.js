$(function() {
    $("#formBuild").form({
        'formName': {
            identifier: 'formName',
            rules: [{
                type: 'empty',
                prompt: 'Campo de preenchimento obrigatório'
            }]
        }
    },  {on: 'blur'});

    $("#formBuild").submit(function(){
        var $elements = $('#elements > div'),
            $h3 = $('#elements h3'),
            isValid = true;
        
        $h3.css('border-color', '#ccc');
    
        if ($elements.length == 0) {
            $h3.css('border-color', 'red');
            isValid = false;
            return false;            
        }
                   
        $elements.each(function(){
            var $name = $(this).find('.field.name'),
                $length = $(this).find('.field.length'),
                nameValue = $name.find('input[type=text]').val();
                lengthValue = $length.find('input[type=text]').val();
            
            $name.removeClass('error');
            $length.removeClass('error');

            if (! nameValue) {
                if (isValid) {
                    $name.find('input[type=text]').focus();
                }
                
                $name.addClass('error');
                isValid = false;
            }
            
            if (! lengthValue && $length.length == 1) {
                if (isValid) {
                    $length.find('input[type=text]').focus();
                }
                
                $length.addClass('error');
                isValid = false;
            }
        });
        
        return isValid;
    });
    
    function changeAdapter() {
        var $parent = $(this).parents('.form'),
            $options = $parent.find('.options'),
            $form = $parent.find('.form'),
            $reference = $parent.find('.reference');
        
        $options.hide().find('.tagit-hidden-field').tagit('removeAll');
        $form.hide().find('.dropdown').dropdown('restore defaults');
        $reference.hide().find('.dropdown').dropdown('restore defaults');
            
        if ($(this).dropdown("get value") == 'static') {
            $options.show(); 
        } else if ($(this).dropdown("get value") == 'form') {
            $form.show();
        } else {
            $reference.show();
        }
    }
    
    var dynamicOptions = {
        onEnable: function(){
            $(this).parents('.two').find('input[type=text]').prop('disabled', false);
        },
        onDisable: function(){
            $(this).parents('.two').find('input[type=text]').prop('disabled', true);
        }
    };
    
    $( "#origin" ).sortable({
        connectWith: "#elements",
        opacity: 0.5,
        placeholder: "ui segment tertiary blue inverted elements-spaced",
        forcePlaceholderSize: false,
        helper: function(e, ui) {
            copyHelper = ui.clone().insertAfter(ui);
            return ui.clone();
        },
        stop: function() {
            copyHelper && copyHelper.remove();
        }
    });
    
    $("#elements").sortable({
        placeholder: "ui segment tertiary blue inverted elements-spaced",
        receive: function(e,ui) {
            var type = ui.item.find('input').val();
                $template = $($('#template-element-' + type).html()),
                count = $('#elements').children().length;
            
            $('#elements h3').hide();
            
            ui.item.html($template.html());
            
            ui.item.find('.content').addClass('active');
            ui.item.find('.options input').tagit({singleField: true, allowSpaces: true});
            ui.item.find('.accordion').accordion();
            ui.item.find('.dropdown').dropdown();
            ui.item.find('.checkbox, .radio').checkbox();
            ui.item.find('.length input').maskMoney({precision: 0, allowNegative: false, thousands:'', decimal:'', affixesStay: false});
            ui.item.find('.adapter .ui.selection.dropdown').change(changeAdapter);
            ui.item.find('.dynamic .checkbox').checkbox(dynamicOptions);
            
            ui.item.find('input.name').keyup(function(){
                ui.item.find('.additional').text(' - ' + this.value);
            });
            
            ui.item.find('input').each(function(){
                this.name = this.name.replace('fields[][0]','fields[][' + (count-1) + ']');
            });
            
            copyHelper = null;
        }
    });
    
    $('.ui.selection.dropdown').dropdown();
    $('.accordion').accordion();
    $('.checkbox, .radio').checkbox();
    $('.options input').tagit({singleField: true, allowSpaces: true});
    $('.length input').maskMoney({precision: 0, allowNegative: false, thousands:'', decimal:'', affixesStay: false});
    $('.adapter .ui.selection.dropdown').change(changeAdapter);
    $('.dynamic .checkbox').checkbox(dynamicOptions);
    
    $('#elements').on('keyup', 'input.name' , function(){
        var nativeName = this.value.split(' ').join('_').toLowerCase().latinise().replace(/[^\w+]/gi,'');
        
        $(this).parents('.accordion').find('.additional').text(' - ' + this.value);
        $(this).parents('.accordion').find('.nativeName').val(nativeName);
    });
    
    $('#elements').on('click', '.remove a', function(){
        $(this).parents('.ui.raised.segment').remove();
        
        if ($('#elements div').length == 0) {
            $('#elements h3').css('border-color', '#ccc').show();
        }
        
        return false;
    });

});