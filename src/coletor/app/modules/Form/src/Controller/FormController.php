<?php

namespace Form\Controller;

use Form\Service\Sisrel;

class FormController extends \Intelletto\Webservice\Mvc\Controller
{   
    public function initialize()
    {
        $this->view->header = "form";
    }
    
    public function indexAction($groupId = 0)
    {
        $groups = $this->getGroups();
        
        if (! $groupId) {
            $groupId = $groups[0]['id'];
        }
        
        $group = array_filter($groups, function($value) use ($groupId) {
            return ($groupId == $value['id']);
        });
        
        $page     = $this->request->get('page');
        $perPage  = $this->request->get('perPage');
        $response = $this->form->getByGroup($groupId);
        
        $this->view->data    = $response->getData()['form'];
        $this->view->groups  = $groups;
        $this->view->group   = current($group);
        $this->view->groupId = $groupId;
    }
	
    public function formAction($groupId = 0, $id = 0)
    {
        $data = [
            'id' => null,
            'name' => null,
            'group_id' => $groupId,
            'layout' => '',
            'struct' => '[]',
            'roles' => '[]'
        ];
        
        if ($id) {
            $response = $this->form->get($id);
            $data     = $response->getData()['form'];
        }
        
        $this->view->metadata = [
            'text' => ['label' => 'Texto', 'icon' => 'font', 'native' => 'varchar'],
            'map' => ['label' => 'Mapa', 'icon' => 'map marker', 'native' => 'varchar'],
            'date' => ['label' => 'Data', 'icon' => 'calendar', 'native' => 'timestamp'],
            'decimal' => ['label' => 'Real', 'icon' => 'dollar', 'native' => 'numeric'],
            'number' => ['label' => 'Inteiro', 'icon' => 'info letter', 'native' => 'integer'],
            'file' => ['label' => 'Arquivo', 'icon' => 'file', 'native' => 'varchar'],
            'options' => ['label' => 'Opções', 'icon' => 'reorder', 'native' => 'varchar']
        ];
        
        $this->view->roles   = (array) $this->aclService->roles()->getData()['role'];
        $this->view->id      = $id;
        $this->view->data    = $data;
        $this->view->forms   = $this->form->all()->getData()['form'];
        $this->view->fields  = json_decode($data['struct']);
        $this->view->reports = (new Sisrel)->getReports($this->config->sisrel->url);
    }
	
    public function saveAction()
    {
        $id      = isset($_POST['id']) ? $_POST['id'] : 0;
        $groupId = (int) $_POST['group_id'];
        $name    = $_POST['formName'];
        $layout  = $_POST['layout'];
        $roles   = isset($_POST['roles']) ? $_POST['roles'] : array();
        $fields  = [];
        $i       = -1;
        $key     = null;

        if (!$layout) {
            $layout = 'auto';
        }
        
        foreach ($_POST['fields'] as $value) {
            $field = current($value);
            
            if ($key != key($value)) {
                $i++;
            }
            
            $fields[$i][key($field)] = current($field);
            $key   = key($value);
        }
        
        $fields = array_values($fields);
        
        $data = [
            'name'     => $name, 
            'group_id' => $groupId, 
            'struct'   => json_encode($fields), 
            'layout'   => $layout,
            'roles'    => $roles
        ];

        if ($id) {
            $response = $this->form->update($id, $data);
        } else {
            $response = $this->form->insert($data);
            $id = $response->getData()['form']['id'];
        }
        
        if ($response->isError()) {
            $message = 'Falha ao salvar.';
            
            if ($this->config->app->debug) {
                $message .= " " . $response->getErrorMessage();
            }
            
            $this->flashSession->error($message);
            return $this->response->redirect('form/form/form/' . $groupId . '/' . $id);
        }
        
        $this->flashSession->success('Realizado com sucesso.');
        return $this->response->redirect('form/form/form/' . $groupId . '/' . $id);
    }
	
    public function deleteAction($id)
    {
        $response = $this->form->delete($id);
        
        if ($response->isError()) {
            $this->flashSession->error('Registro inválido.');
            return $this->response->redirect('form/form/index/');
        }
        
        $this->flashSession->success('Realizado com sucesso.');
        return $this->response->redirect('form/form/index/');
    }
    
    protected function getGroups()
    {
        return $this->group->all()->getData()['group'];
    }
}