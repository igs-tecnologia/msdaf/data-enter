<?php

namespace Form\Controller;

class GroupController extends \Intelletto\Webservice\Mvc\Controller
{   
    public function initialize()
    {
        $this->view->header = "form";
    }
    
    public function formAction($id = 0)
    {
        $data = [
            'id' => null,
            'name' => null,
        ];
        
        if ($id) {
            $response = $this->group->get($id);
            $data     = $response->getData()['group'];
        }
        
        $this->view->id   = $id;
        $this->view->data = $data;
    }
	
    public function saveAction()
    {
        $id   = isset($_POST['id']) ? $_POST['id'] : 0;
        $data = ['name' => $_POST['name']];
        
        if ($id) {
            $response = $this->group->update($id, $data);
        } else {
            $response = $this->group->insert($data);
            $id = $response->getData()['group']['id'];
        }
        
        if ($response->isError()) {
            $this->flashSession->error('Falha ao salvar.');
            return $this->response->redirect('form/group/form/');
        }
        
        $this->flashSession->success('Realizado com sucesso.');
        return $this->response->redirect('form/form/index/' . $id);
    }
	
    public function deleteAction($id)
    {
        $response = $this->group->delete($id);
        
        if ($response->isError()) {
            $this->flashSession->error('Registro inválido.');
            return $this->response->redirect('form/form/index/');
        }
        
        return $this->response->redirect('form/form/index/');
    }
}