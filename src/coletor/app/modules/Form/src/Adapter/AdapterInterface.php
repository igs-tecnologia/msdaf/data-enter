<?php

namespace Form\Adapter;

interface AdapterInterface
{
    public function __construct(array $struct);
    public function getOptions($parentValue);
}