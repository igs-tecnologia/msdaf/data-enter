<div class="column wide three">
    <div class="ui small fluid vertical pointing admin menu">
        <div class="item">
            <a href="form/group/form" class="fluid mini ui button group-add"><i class="add icon"></i> Criar grupo</a>
        </div>

        {% for row in groups %}
            {% if row['id'] == groupId %}
                <a class="item active" href="form/form/index/{{row['id']}}">
                    <div class="ui small label black">{{row['forms']}}</div>
                    {{row['name']}}
                </a>
            {% else %}
                <a class="item" href="form/form/index/{{row['id']}}">
                    <div class="ui small label">{{row['forms']}}</div>
                    {{row['name']}}
                </a>
            {% endif %}
        {% endfor %}
    </div>
</div>

<div class="column wide thirteen">
    <h3 class="body-title-page" style="margin-bottom:30px">
        {{group['name']}}
        <div class="small" style="display:inline-block;margin-left:15px;vertical-align:middle">   
            <a href="form/group/form/{{groupId}}" class="group-edit link-icon" title="Editar grupo"><i class="small edit link icon"></i></a>
            <a href="form/group/delete/{{groupId}}" class="link-icon delete" title="Remover grupo"><i class="small remove link icon"></i></a>
        </div>   
    </h3>
    {% include '../../../layouts/message.volt' %}
    <div class="ui" style="margin-bottom:30px;overflow:hidden">
        <a href="form/form/form/{{ group['id'] }}" class="ui small right floated submit button"><i class="add icon"></i> Novo formulário</a>
    </div>

    <table class="ui basic small table" style="margin-bottom:30px">
        <thead>
            <tr>
                <th>Título do formulário</th>
                <th>Data da criação</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            {% if data|length %}
                {% for row in data %}
                    <tr>
                        <td><a href="form/form/form/{{ group['id'] }}/{{ row['id'] }}" class="table-link" title="Editar formulário">{{ row['name'] }}</a></td>
                        <td><?=date('d/m/Y', strtotime($row['created']))?></td>
                        <td style="text-align:right">
                            <a href="form/form/form/{{ group['id'] }}/{{ row['id'] }}" class="link-icon"><i class="edit link icon"></i></a>
                            <a href="form/form/delete/{{ row['id'] }}" class="link-icon delete" ><i class="remove delete link icon"></i></a>
                        </td>
                    </tr>
                {% endfor %}                           
            </tbody>
        {% endif %}
    </table>
</div>

<div class="ui modal modal-group">
    <i class="close icon"></i>
    <div class="header">
        Grupo
    </div>
    <div class="content">
        <form action="form/group/save" method="post">
            <input type="hidden" name="id" value="{{ group['id'] }}">
    		<div class="ui form">
                <div class="field">
                    <label>Nome do grupo:</label>
                    <div class="ui left labeled input">
                        <input maxlength="50" type="text" name="name" value="{{ group['name'] }}">
                        <div class="ui corner label">
                            <i class="icon asterisk"></i>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="actions">
        <div class="ui black button">
            Cancelar
        </div>
        <div class="ui positive right labeled icon button">
            <i class="checkmark icon"></i>
            Salvar
        </div>
    </div>
</div>

<div class="ui modal modal-group-add">
    <i class="close icon"></i>
    <div class="header">
        Grupo
    </div>
    <div class="content">
        <form action="form/group/save" method="post">
            <input type="hidden" name="id">
    		<div class="ui form">
                <div class="field">
                    <label>Nome do grupo:</label>
                    <div class="ui left labeled input">
                        <input maxlength="50" type="text" name="name">
                        <div class="ui corner label">
                            <i class="icon asterisk"></i>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="actions">
        <div class="ui black button">
            Cancelar
        </div>
        <div class="ui positive right labeled icon button">
            <i class="checkmark icon"></i>
            Salvar
        </div>
    </div>
</div>
{% include '../../../layouts/modal-delete.volt' %}

<script type="text/javascript">
    $('.modal form').form({
        'name': {
            identifier: 'name',
            rules: [{
                type: 'empty',
                prompt: 'Campo de preenchimento obrigatório'
            }]
        }
    },  {inline: true, on: 'blur'});
    
    
    $('.group-add').click(function(){
        $('.modal-group-add').modal('show')
                             .modal('setting', {
                                onApprove : function() {
                                    $(this).find('form').submit();
                                }
                             });
        return false;
    });
    
    $('.group-edit').click(function(){
        $('.modal-group').modal('show')
                         .modal('setting', {
                             onApprove : function() {
                               $(this).find('form').submit();
                             }
                         });
        return false;
    });

</script>