<div class="inline field small">
    <div class="ui toggle checkbox">
        {% set checked = null %}
        {% if field.required is defined and field.required %}{% set checked = 'checked' %}{% endif %}
        <input {{ checked }} type="checkbox" name="fields[][{{ i }}][required]" value="1">
        <label>Obrigatório</label>
    </div>
</div>
{# 
<div class="inline field">
    <div class="ui toggle checkbox">
        {% set checked = null %}
        {% if field.unique is defined and field.unique %}{% set checked = 'checked' %}{% endif %}
        <input {{ checked }} type="checkbox" name="fields[][{{ i }}][unique]" value="1">
        <label>Único</label>
    </div>
</div>
#}