<div class="ui form">
    <div class="ui two fields">
        
        {{ partial('form/types/common/name') }}
        
        {% if field.field is defined %}
            <div class="field form">
                <label>Campo</label>
                <div class="ui selection fluid dropdown">
                    <input name="fields[][{{ i }}][field]" type="hidden" value="{{ field.field }}">
                    <div class="default text">Campo</div>
                    <i class="dropdown icon"></i>
                    <div class="menu">
                        <?php foreach($forms as $form): ?>
                            <?php $formFields = json_decode($form['struct'])?>
                            <?php foreach($formFields as $formField): ?>
                                {% if formField.nativeType == 'varchar' and form['id'] != id %}
                                <div class="item" data-value="{{ form['id'] ~ '.' ~ formField.nativeName }}">{{ form['name'] ~ ' - ' ~ formField.name }}</div>
                                {% endif %}
                            <?php endforeach ?>
                        <?php endforeach ?>
                    </div>
                    <div class="ui corner label">
                        <i class="icon asterisk"></i>
                    </div>
                </div>
            </div>
        {% endif %}
    </div>
    
    {{ partial('form/types/common/description') }}
    <div class="ui two fields">
        {{ partial('form/types/common/checks') }}
    </div>
</div>