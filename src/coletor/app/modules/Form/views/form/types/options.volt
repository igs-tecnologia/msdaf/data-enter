<div class="ui form">
    <div class="ui two fields">
        {{ partial('form/types/common/name') }}
        <div class="field adapter">
            <label>Fonte de dados</label>
            <div class="ui selection fluid dropdown">
                <input name="fields[][{{ i }}][adapter]" type="hidden" value="{% if field.adapter is defined %}{{ field.adapter }}{% endif %}">
                <div class="default text">Fonte de dados</div>
                <i class="dropdown icon"></i>
                <div class="menu">
                    <div class="item" data-value="static">Estática</div>
                    <div class="item" data-value="form">Formulário</div>
                    <div class="item" data-value="sisrel">Sisrel</div>
                </div>
                <div class="ui corner label">
                    <i class="icon asterisk"></i>
                </div>
            </div>
        </div>
    </div>
    
    <div class="field options" {% if !(field.options is defined and field.options) %}style="display:none"{% endif %}>
        <label>Opções</label>
        <div class="ui left labeled input">
            <input value="{% if field.options is defined %}{{ field.options }}{% endif %}" name="fields[][{{ i }}][options]" placeholder="Opções" type="text">
            <div class="ui corner label">
                <i class="icon asterisk"></i>
            </div>
        </div>
    </div>
    
    <div class="field reference" {% if !(field.reference is defined and field.reference) %}style="display:none"{% endif %}>
        <label>Relatório</label>
        <div class="ui selection fluid dropdown">
            <input name="fields[][{{ i }}][reference]" type="hidden" value="{% if field.reference is defined %}{{ field.reference }}{% endif %}">
            <div class="default text">Relatório</div>
            <i class="dropdown icon"></i>
            <div class="menu">
                {% for report in reports %}
                    <div class="item" data-value="{{ report['co_consulta'] ~ '.' ~ report['field'] }}">{{ report['no_consulta'] ~ ' - ' ~ report['description'] }}</div>
                {% endfor %}
            </div>
            <div class="ui corner label">
                <i class="icon asterisk"></i>
            </div>
        </div>
    </div>
    
    <div class="field form" {% if !(field.field is defined and field.field) %}style="display:none"{% endif %}>
        <label>Campo</label>
        <div class="ui selection fluid dropdown">
            <input name="fields[][{{ i }}][field]" type="hidden" value="{% if field.field is defined %}{{ field.field }}{% endif %}">
            <div class="default text">Campo</div>
            <i class="dropdown icon"></i>
            <div class="menu">
                <?php foreach($forms as $form): ?>
                    <?php $formFields = json_decode($form['struct'])?>
                    <?php foreach($formFields as $formField): ?>
                        {% if formField.nativeType == 'varchar' and form['id'] != id %}
                        <div class="item" data-value="{{ form['id'] ~ '.' ~ formField.nativeName }}">{{ form['name'] ~ ' - ' ~ formField.name }}</div>
                        {% endif %}
                    <?php endforeach ?>
                <?php endforeach ?>
            </div>
            <div class="ui corner label">
                <i class="icon asterisk"></i>
            </div>
        </div>
    </div>
    
    {{ partial('form/types/common/description') }}
    
    <div class="ui two fields">
        <div class="field dynamic">
            <label>&nbsp;</label>
            <div class="ui toggle checkbox">
                {% set checked = null %}
                {% if field.dynamic is defined and field.dynamic %}{% set checked = 'checked' %}{% endif %}
                <input {{ checked }} type="checkbox" name="fields[][{{ i }}][dynamic]" value="1">
                <label>Carregamento dependente</label>
            </div>
        </div>
        <div class="field">
            <label>Campo de filtro</label>
            <div class="ui left labeled input">
                {% set disabled = 'disabled' %}
                {% if field.dynamic is defined and field.dynamic %}{% set disabled = null %}{% endif %}
                <input {{ disabled }} value="{% if field.filter is defined %}{{ field.filter }}{% endif %}" name="fields[][{{ i }}][filter]" placeholder="Filtro" type="text">
            </div>
        </div>
    </div>
    
    <div class="ui two fields">
        {{ partial('form/types/common/checks') }}
    </div>
</div>