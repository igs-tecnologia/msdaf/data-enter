<div class="ui form">
    <div class="ui two fields">
        {{ partial('form/types/common/name') }}
        
        {% if field.field is defined %}
            <div class="field hidden">
                <label>Campo</label>
                <div class="ui selection fluid dropdown">
                    <input name="fields[][{{ i }}][field]" type="hidden" value="{{ field.field }}">
                    <div class="default text">Campo</div>
                    <i class="dropdown icon"></i>
                    <div class="menu">
                        <div class="item" data-value="userName">Nome de usuário</div>
                        <div class="item" data-value="uf">UF</div>
                        <div class="item" data-value="no">NO</div>
                        <div class="item" data-value="area">Área</div>
                    </div>
                    <div class="ui corner label">
                        <i class="icon asterisk"></i>
                    </div>
                </div>
            </div>
        {% endif %}
    </div>
    
    {{ partial('form/types/common/description') }}
</div>