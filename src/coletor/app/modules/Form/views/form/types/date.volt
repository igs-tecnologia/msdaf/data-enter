<div class="ui form">
    <div class="ui two fields">
        {{ partial('form/types/common/name') }}
    </div>
    
    {{ partial('form/types/common/description') }}
    
    <div class="ui two fields">
        {{ partial('form/types/common/checks') }}
    </div>
</div>