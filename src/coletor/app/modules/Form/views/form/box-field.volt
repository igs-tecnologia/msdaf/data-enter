<div class="ui raised segment">
    <div class="ui right remove floated header">
        <a class="link-text" href="#remove"><i class="icon small link remove close"></i></a>
    </div>
    <div class="ui accordion basic">
        <div class="title">
            <i class="icon dropdown"></i>
			<i class="{{ metadata[field.type]['icon'] }} icon"></i> 
            {{ metadata[field.type]['label'] }}
            <span class="additional"> {% if field.name %}- {{ field.name }}{% endif %}</span>
            <input class="type" type="hidden" name="fields[][{{ i }}][type]" value="{{ field.type }}">
            <input type="hidden" name="fields[][{{ i }}][nativeType]" value="{{ field.nativeType }}">
            <input type="hidden" name="fields[][{{ i }}][nativeName]" value="{{ field.nativeName }}" {% if not isEditing %} class="nativeName" {% endif %}>
        </div>
        <div class="content">
            {% if field.type is defined %}
                {{ partial('form/types/' ~ field.type) }}
            {% endif %}
        </div>
    </div>
</div>