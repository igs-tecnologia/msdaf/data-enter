<script type="text/html" id="template-element-text">
    <?php $field = (object) ['type' => 'text', 'nativeType' => 'varchar', 'name' => '', 'nativeName' => '', 'length' => 255, 'description' => '']; ?>
    {% set i = 0 %}
    {{ partial('form/box-field') }}
</script>

<script type="text/html" id="template-element">
<div class="ui right remove floated header">
    <a class="link-text" href="#remove"><i class="icon remove link icon close"></i></a>
</div>
<div class="ui accordion basic">
    <div class="active title">
        <i class="icon dropdown"></i>
    </div>
    <div class="active content">
        <div class="ui form">
            <div class="ui two fields">
                <div class="field name">
                    <label>Nome</label>
                    <div class="ui left labeled input">
                        <input class="name" name="fields[][name]" placeholder="Nome" type="text">
                        <div class="ui corner label">
                            <i class="icon asterisk"></i>
                        </div>
                    </div>
                </div>
                <div class="field length">
                    <label>Tamanho</label>
                    <div class="ui left labeled input">
                        <input name="fields[][length]" placeholder="Tamanho" type="text">
                        <div class="ui corner label">
                            <i class="icon asterisk"></i>
                        </div>
                    </div>
                </div>
                <div class="field forms">
                    <label>Campo</label>
                    <div class="ui selection fluid dropdown">
                        <input name="fields[][field]" type="hidden" value="">
                        <div class="default text">Campo</div>
                        <i class="dropdown icon"></i>
                        <div class="menu">
                            {% for form in forms %}
                                <?php $formFields = json_decode($form['struct'])?>
                                {% for formField in formFields %}
                                    {% if formField.nativeType == 'varchar' and form['id'] != id %}
                                        <div class="item" data-value="{{ form['id'] ~ '.' ~ formField.nativeName }}">{{ form['name'] ~ ' - ' ~ formField.name }}</div>
                                    {% endif %}
                                {% endfor %}
                            {% endfor %}
                        </div>
                        <div class="ui corner label">
                            <i class="icon asterisk"></i>
                        </div>
                    </div>
                </div>
                <div class="field adapter">
                    <label>Fonte de dados</label>
                    <div class="ui selection fluid dropdown">
                        <input name="fields[][adapter]" type="hidden" value="">
                        <div class="default text">Fonte de dados</div>
                        <i class="dropdown icon"></i>
                        <div class="menu">
                            <div class="item" data-value="static">Estática</div>
                            <div class="item" data-value="sisrel">Sisrel</div>
                        </div>
                        <div class="ui corner label">
                            <i class="icon asterisk"></i>
                        </div>
                    </div>
                </div>
                <div class="field hidden">
                    <label>Campo</label>
                    <div class="ui selection fluid dropdown">
                        <input name="fields[][field]" type="hidden" value="">
                        <div class="default text">Campo</div>
                        <i class="dropdown icon"></i>
                        <div class="menu">
                            <div class="item" data-value="userName">Nome de usuário</div>
                            <div class="item" data-value="uf">UF</div>
                            <div class="item" data-value="no">NO</div>
                            <div class="item" data-value="area">Área</div>
                        </div>
                        <div class="ui corner label">
                            <i class="icon asterisk"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="field options" style="display: none;">
                <label>Opções</label>
                <div class="ui left labeled input">
                    <input name="fields[][options]" placeholder="Opções" type="text">
                </div>
            </div>
            <div class="field reference" style="display: none;">
                <label>Referência</label>
                <div class="ui left labeled input">
                    <input name="fields[][reference]" placeholder="Referência" type="text">
                </div>
            </div>
            <div class="field">
                <label>Descrição</label>
                <div class="ui left labeled input">
                    <input name="fields[][description]" placeholder="Descrição" type="text">
                </div>
            </div>
            <div class="ui two fields checks">
                <div class="inline field">
                    <div class="ui checkbox">
                        <input type="checkbox" name="fields[][required]" value="1">
                        <label>Obrigatório</label>
                    </div>
                </div>
                <div class="inline field">
                    <div class="ui checkbox">
                        <input type="checkbox" name="fields[][unique]" value="1">
                        <label>Único</label>
                    </div>
                </div>
            </div>
        </div>    
    </div>
</div>  
</script>