<div class="column wide sixteen">
    <div class="ui icon small floating yellow message default-message">
        <i class="ban circle icon"></i>
        <div class="content">
            <p class="header default-message-title">
                Acesso negado
            </p>
            <p>
                Você não tem permissão para acessar esta área. Qualquer dúvida, entre em contato com o administrador do sistema.
            </p>
        </div>
    </div>
</div>