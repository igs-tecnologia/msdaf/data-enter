<div class="column wide sixteen">
    <form id="formGroup" action="form/group/save" method="POST">
        <input type="hidden" name="id" value="{{ data['id'] }}">
		<div class="ui form">
            <div class="field">
                <label>Nome do grupo:</label>
                <div class="ui left labeled input">
                    <input maxlength="50" type="text" name="name" value="{{ data['name'] }}">
                    <div class="ui corner label">
                        <i class="icon asterisk"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="ui segment basic floated right" style="margin:0;padding:0">
            <a href="form/form/index">voltar</a> &nbsp; <input class="ui button submit" value="Salvar" type="submit">
        </div>
    </form>
</div>
<script type="text/javascript">
    $("#formGroup").form({
        'name': {
            identifier: 'name',
            rules: [{
                type: 'empty',
                prompt: 'Campo de preenchimento obrigatório'
            }]
        }
    },  {inline: true, on: 'blur'});
</script>