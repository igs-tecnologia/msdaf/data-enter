{% include 'data/menu.volt' %}
<div class="column wide thirteen">
    <h3 class="body-title-page" style="margin-bottom:30px">{{ group['name'] }}</h3>
    
    <table class="ui small basic table" style="margin-bottom:30px">
        <thead>
            <tr>
                <th>Formulário</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            {% if forms|length > 0 %}
                {% for form in forms if session.identity['role_id'] in form['roles'] %}
                    <tr>
                        <td>
                            <a href="form/data/view/{{ group['id'] }}/{{ form['id'] }}" class="table-link" title="Listar conteúdo">{{ form['name'] }}</a>
                        </td>
                        <td style="text-align:right">
                            <a href="form/data/view/{{ group['id'] }}/{{ form['id'] }}" class="detail ui basic button mini">Listar conteúdo</a>
                        </td>
                    </tr>
                {% endfor %}
            {% else %}
                <tr>
                    <td colspan="2">Nenhum formulário cadastrado para o grupo.</td>
                </tr>
            {% endif %}
        </tbody>
    </table>
</div>