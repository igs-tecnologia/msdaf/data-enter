<div class="column wide three">
    <div class="ui small fluid vertical pointing admin menu">
        {% for row in groups %}
            <a class="item {% if row['id'] == groupId %}active{% endif %}" href="form/data/index/{{ row['id'] }}">
                {{ row['name'] }}
            </a>
        {% endfor %}
    </div>
</div>