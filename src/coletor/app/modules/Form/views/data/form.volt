{% set latitude = "-15.792253570362446" %}
{% set longitude = "-47.900390625" %}
{% include 'data/menu.volt' %}
<script type="text/javascript">
    var validate = {};
</script>
<div class="column wide thirteen data-form">
    <h3 class="body-title-page" style="margin-bottom:30px">
        <a href="form/data/view/{{groupId}}/{{form['id']}}" title="Voltar para a página anterior"><i class="large angle left icon"></i></a>{{ form['name'] }}
    </h3>
    {% include '../../../layouts/message.volt' %}
    <form class="ui form" action="form/data/save" method="post" enctype="multipart/form-data">
        <input type="hidden" name="id" value="{{ data['id'] }}">
        <input type="hidden" name="group_id" value="{{groupId}}">
        <input type="hidden" name="form_id" value="{{ form['id'] }}">
        <div class="ui segment form-bg">
            {% if form['layout'] == 'auto' %}
                <div class="three fields">
            {% else %}
                <div>
            {% endif %}
                {% set lastDataValue = null %}
                {% set hasFile = false %}
                {% for field in fields %}
                    {% if field.type == 'hidden' %}{% continue %}{% endif %}
                    
                    <script type="text/javascript">
                        validate.{{ field.nativeName }} = {identifier : '{{ field.nativeName }}', rules: []};
                    </script>
                    
                    {% set dataValue = null %}
                    {% if value[field.nativeName] is defined %}
                        {% set dataValue = value[field.nativeName] %}
                        {% if field.nativeType == 'timestamp' %}
                            <?php $dataValue = date('d/m/Y', strtotime($dataValue)) ?>
                        {% endif %}
                        
                        {% if field.nativeType == 'numeric' %}
                            <?php $dataValue = number_format((float) $dataValue, 2, ',', '.') ?>
                        {% endif %}
                        {% if field.nativeType == 'integer' %}
                            <?php $dataValue = number_format((int) $dataValue, 0, '', '.') ?>
                        {% endif %}
                    {% endif %}
                    <div class="field">
                        <label>
                            {{ field.name }} 
                            {% if field.type == 'file' %}
                                **
                                {% set hasFile = true %}
                            {% endif %}
                        </label>
                        
                        {% if field.type == "map" %}
                            <div class="ui left action labeled input">
                        {% elseif field.type == "decimal" or field.type == "date" %}
                            <div class="ui left icon labeled input">
                        {% else %}
                            <div class="ui left labeled input">
                        {% endif %}
                        
                            {% set maxlength = null %}
                            {% if field.length is defined and field.length %}
                                {% set maxlength = 'maxlength="' ~ field.length ~ '"'  %}
                            {% endif %}
                            
                            {% if field.type == "options" %}
                                <?php $optionId = uniqid('option_'); ?>
                                <div class="ui selection dropdown fluid" id="{{optionId}}">
                                    <input name="data[{{ field.nativeName }}]" type="hidden" value="{{ dataValue }}">
                                    <div class="default text">{{ field.name }}</div>
                                    <i class="dropdown icon"></i>
                                    <div class="menu">
                                        <?php $options = (new Form\Adapter\Factory((array) $field, $di))->getOptions($lastDataValue) ?>
                                        {% for index,option in options %}
                                            <div class="item" data-value="{{ index }}">{{ option }}</div>
                                        {% endfor %}
                                    </div>
                                </div>
                                {% if field.dynamic is defined and field.dynamic %}
                                    <script type="text/javascript">
                                        $("#{{lastOptionId}}").change(function(){
                                            var value = $(this).dropdown('get value');
                                            $("#{{optionId}}").dropdown('set text',"(carregando)");
                                            $("#{{optionId}}").find('.menu').load('form/data/load', {value: value, field: <?=json_encode($field)?>}, function(){
                                                $("#{{optionId}}").dropdown();
                                                $("#{{optionId}}").dropdown('set text',"(selecione)");
                                            });
                                        });
                                    </script>
                                {% endif %}
                                <?php $lastOptionId = $optionId; ?>
                            {% elseif field.type == "file" %}
                                <input multiple class="{{ field.type }}" type="file" name="data[{{ field.nativeName }}][]" id="{{ field.nativeName }}">
                            {% else %}    
                                <input class="{{ field.type }}" {{ maxlength }} type="text" name="data[{{ field.nativeName }}]" value="{{ dataValue }}" id="{{ field.nativeName }}">
                            {% endif %}
                                    
                            {% if field.type == "map" %}
                                {% if value[field.nativeName] is defined %}
                                    <?php $coordinates = explode(",", $dataValue) ?>
                                    {% if coordinates|length > 1 %}
                                        {% set latitude = coordinates[0] %}
                                        {% set longitude = coordinates[1] %}
                                    {% endif %}
                                {% endif %}
                                <a href="#map" title="Abrir mapa" class="ui icon button btn-maps"><i class="icon map marker"></i></a>
                                <script type="text/javascript">
                                    validate.{{ field.nativeName }}.rules.push({
                                        type: 'coordinates',
                                        prompt: 'Coordenadas inválidas'
                                    });
                                </script>
                            {% endif %}
                            
                            {% if field.type == "date" %}
                                <i class="calendar icon"></i>
                                <script type="text/javascript">
                                    validate.{{ field.nativeName }}.rules.push({
                                        type: 'date',
                                        prompt: 'Data inválida'
                                    });
                                </script>
                            {% endif %}
                            
                            {% if field.required is defined and field.required %}
                                <div class="ui corner label">
                                    <i class="icon asterisk"></i>
                                </div>
                                <script type="text/javascript">
                                    validate.{{ field.nativeName }}.rules.push({
                                        type: 'empty',
                                        prompt: 'Campo de preenchimento obrigatório'
                                    });
                                </script>
                            {% endif %}
                        </div>
                    </div>
                    {% if dataValue and field.type == "file" %} 
                        <?php $files = explode(",", $dataValue) ?>
                        <div class="field files" style="{% if form['layout'] == 'auto' %}clear: both; width: auto;{% endif %}">
                            <label>Arquivos enviados</label>
                            <div class="ui small labels">
                                {% for file in files %}
                                    <div class="ui image label">
                                        <a title="Ver arquivo" style="text-decoration: none;" target="_blank" href="form/data/file/{{ file }}">{{ file }}</a>
                                        <i title="Remover" class="delete icon"></i>
                                        <input name="data[{{ field.nativeName }}][]" type="hidden" value="{{ file }}">
                                    </div>
                                {% endfor %}
                            </div>
                        </div>
                    {% endif %}
                    
                    {% set lastDataValue = dataValue %}
                {% endfor %}
            </div>
            {% if hasFile %}
                <p style="font-size: .7rem">** O tamanho de cada arquivo deve ser no máximo de {{filesize}} e um total de {{maxsize}}</p>
            {% endif %}
        </div>
        {% if form['layout'] == 'auto' %}
            <div class="ui error message"></div>
        {% endif %}
        <div class="ui segment basic floated right" style="margin:0;padding:0">
            <a href="form/data/view/{{groupId}}/{{form['id']}}" class="link-text">voltar</a> &nbsp; 
            <!--input class="ui button submit" value="Salvar" type="submit"-->
            <button class="ui button" type="submit" name="btnSalvar"  value="1"> Salvar</button>
            <button class="ui black button" type="submit" name="btnSalvar"  value="2"> Salvar e Criar Novo</button>
        </div>
    </form>
</div>

<div class="ui modal modal-map">
    <i class="close icon"></i>
    <div class="header">
        Google Maps
    </div>
    <div class="content">
        
    </div>
    <div class="actions">
        <div class="ui button cancel">Cancelar</div>
        <div class="ui button ok">OK</div>
    </div>
</div>

<div id="maps" style="position: absolute; top:-9999px;">
    <div class="gllpLatlonPicker">
        <div class="gllpMap">Google Maps</div>
        <input type="hidden" id="latitude" class="gllpLatitude" value="{{ latitude }}">
        <input type="hidden" id="longitude" class="gllpLongitude" value="{{ longitude }}">
        <input type="hidden" class="gllpZoom" value="3">
        <input type="hidden" class="gllpLocationName"><br>
        <input type="text" class="input-xlarge gllpSearchField">
        <input type="button" class="btn btn-small gllpSearchButton" value="Buscar">
    </div>
</div>

<script type="text/javascript">
    {% if form['layout'] == 'auto' %}
        $('.ui.form').form(validate,  {on: 'blur'});
    {% else %}
        $('.ui.form').form(validate, {inline: true, on: 'blur'});
    {% endif %}
    
    $('input.date').datepicker({
        changeYear: true
    });
    $('input.date').mask("00/00/0000", {placeholder: "__/__/____"});
    $("input.decimal").maskMoney({allowNegative: false, thousands:'.', decimal:',', affixesStay: false});
        console.log($("input.number"));
    $("input.number").maskMoney({precision: 0, allowNegative: false, thousands:'.', decimal:'', affixesStay: false});
    $('.ui.selection.dropdown').dropdown();
    
    $('.btn-maps').click(function(){
        var $this = $(this);
        
        $('.modal-map').find('.content').append($(".gllpLatlonPicker"));
        
        $('.modal-map').modal('setting', {
            onApprove: function() {
                $this.parent().parent().find('input').val($('#latitude').val() + ',' + $('#longitude').val());
            }
        }).modal('show');
        return false;
    });
    
    $('.files .delete').click(function(){
        if (confirm('Deseja realmente excluir o arquivo?')) {
            var $this = $(this);
                
            $this.parent().remove();
        }
    });
</script>
