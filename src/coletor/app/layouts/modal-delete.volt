<div class="ui small modal modal-delete" style="margin-top: -1px;">
    <i class="close icon"></i>
    <div class="header">
        Confirmar exclusão
    </div>
    <div class="content">
        <p id="txt-confirm"></p>
    </div>
    <div class="actions">
        <div class="ui negative button">
            Não
        </div>
        <div class="ui positive right labeled icon button">
            Sim
            <i class="checkmark icon"></i>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        $('a.delete').click(function(){
            var url = this.href;
            $("#txt-confirm").html("Tem certeza que deseja excluir este registro?");

            $('.modal-delete')
                    .modal('setting', {
                        onApprove : function() {
                            window.location = url;
                        }
                    })
                    .modal('show')
            ;
            return false;
        });
    });
</script>