<div class="ui attached message">
    <div class="header">
        Recuperar senha
        <a href="idp" class="ui black label" style="float:right;text-decoration:none">
            Entrar
        </a>    
    </div>
</div>
<form action="idp/recovery/send" method="POST" class="ui form attached fluid segment">
    <div class="field">
		<div class="ui error message"></div>
        <label>Email</label>
        <div class="ui left labeled input">
            <input placeholder="" type="text" name="email" id="email">
            <div class="ui corner label">
                <i class="icon asterisk"></i>
            </div>
        </div> 
    </div>
    <input type="submit" class="ui blue submit button" value="Enviar">
</form>
<script type="text/javascript" src="js/idp/recovery.js"></script>