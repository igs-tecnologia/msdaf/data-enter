<div class="ui attached message">
    <div class="header left floated">
        Login
        <!--
        <a href="cadastro.html" class="ui black label" style="float:right">
            Cadastre-se
        </a>
        -->
    </div>
</div>
<form action="idp/login/login" method="post" class="ui form attached fluid segment">
    {{ flashSession.output() }}
    <div class="ui error message"></div>
    <div class="field">
        <label>Usuário</label>
        <div class="ui left labeled icon input">
            <input type="text" name="identity" id="identity">
            <i class="user icon"></i>
            <div class="ui corner label">
                <i class="icon asterisk"></i>
            </div>
        </div> 
    </div>
    <div class="field">
        <label>Senha</label>
        <div class="ui left labeled icon input">
            <input type="password" name="secret" id="secret">
            <i class="lock icon"></i>
            <div class="ui corner label">
                <i class="icon asterisk"></i>
            </div>
        </div>  
    </div>
    <input type="submit" class="ui blue submit button" value="Entrar">
</form>
<div class="ui bottom attached info message" style="background:#f9f9f9">
    <i class="icon help"></i>
    <a href="idp/recovery" style="color:#666;font-size:12px;text-decoration:none">Esqueceu sua senha?</a>
</div>
<script type="text/javascript" src="js/idp/login.js"></script>
