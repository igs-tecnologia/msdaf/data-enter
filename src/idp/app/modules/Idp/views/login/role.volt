<div class="column">
    <div class="ui attached message">
        <div class="header left floated">Primeiro acesso</div>
    </div>
    <div class="ui form attached fluid segment">
        <div class="ui yellow message">
            Você possui mais de um perfil. Escolha o perfil desejado para o seu primeiro acesso. Você poderá trocar de perfil sempre que necessário.
        </div>
        <div class="ui list">
            {% for role in roles %}
            <a class="item" href="{{ url }}/{{ token }}/{{ role['id'] }}" style="text-decoration: none;">
                <i class="right triangle icon"></i>{{ role['name'] }}
            </a>
            {% endfor %}
        </div>
    </div>
</div>