<div class="ui attached message">
    <div class="header left floated">
        Escolha o Sistema que deseja acessar:
    </div>
</div>
<div class="ui form attached fluid segment">
    <ul>
        {% for system in systems %}
            <li><a href="idp/login/index/{{ system['id'] }}">{{ system['name'] }}</a></li>
        {% endfor %}
    </ul>
</div>