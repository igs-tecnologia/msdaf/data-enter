<?php
namespace Idp\Controller;

use Idp\Model\System;

class LoginController extends \Phalcon\Mvc\Controller
{
    public function indexAction($systemId)
    {
        if (! $systemId) {
            $systemId = $this->config->idp->dashboard_id;
        }

        $this->session->set('system_id', $systemId);

        if ($this->session->get('identity')) {
            if (! $this->flashSession->has('error')) {
                return $this->response->redirect('idp/login/role');
            }
        }
    }

    public function loginAction()
    {
        if (! $this->request->isPost()) {
            return $this->response->redirect('idp/login/index');
        }

        $identity = $this->request->getPost('identity');
        $secret   = $this->request->getPost('secret');
        $login    = $this->authentication->login($identity, $secret);
        $systemId = $this->session->get('system_id');

        if (!$systemId
            || $this->isInvalidResponse($login)
            || $this->isInvalidSystem($systemId))
        {
            return $this->response->redirect('idp/login/index/' . $systemId . '?login-invalid');
        }

        $loginData = $login->getData();

        $this->session->set('identity', [
            'identity' => $identity,
            'name' => $loginData['login']['name'],
            'user_id' => $loginData['login']['user_id'],
            'token' => $loginData['login']['token']
        ]);

        $this->session->set('X_AUTH_SYSTEM', $systemId);

        $this->dispatcher->forward([
            "module" => "idp",
            "controller" => "login",
            "action" => "role"
        ]);
    }

    public function logoutAction($systemId)
    {
        $this->authentication->logout($this->session->get('identity')['user_id']);
        $this->session->destroy();
        foreach ($_COOKIE as $key => $value) {
            unset($value);
            setcookie($key, '', time() - 3600);
        }

        return $this->response->redirect('idp/login/index/'  . $systemId);
    }

    public function roleAction()
    {
        $systemId  = $this->session->get('system_id');
        $token     = $this->session->get('identity')['token'];
        $userId    = $this->session->get('identity')['user_id'];

        if (! $systemId) {
            $systemId = $this->config->idp->system_id;
        }

        if ($this->isInvalidSystem($systemId)) {
            return $this->response->redirect('idp/login/index/' . $systemId . '?role-invalidSystem');
        }
        
        $returnUrl = $this->systemData->return_url;

        $roles = $this->aclService->userRoles($systemId);
        $role = $this->role->last($userId, $systemId);

        if ($this->isInvalidResponse($roles) || $this->isInvalidResponse($role)) {
            $this->session->remove('identity');
            return $this->response->redirect('idp/login/index/' . $systemId . '?role-invalidResponse');
        }

        $rolesData = $roles->getData()['roles'];
        $roleId = $role->getData()['role_id'];

        //@todo Adicionar página de seleção de sistema baseado no usuário logado
        if ($this->systemData->return_url == '-') {
            $this->flashSession->error('Acesso inválido. Acesse por meio de um sistema válido.');

            return $this->response->redirect('idp/login/index/' . $systemId);
        }
        
        //Usuário com um único perfil
        if (count($rolesData) === 1) {
            $url = sprintf("%s/%s/%s", $returnUrl, $token, $rolesData[0]['id']);
            return $this->response->redirect($url, true);
        }
        
        //Verificação de último perfil logado para o sistema
        if ($roleId) {
            $url = sprintf("%s/%s/%s", $returnUrl, $token, $roleId);
            return $this->response->redirect($url, true);
        }

        //Primeiro acesso, escolhe o perfil
        $this->view->url   = $this->systemData->return_url;
        $this->view->token = $token;
        $this->view->roles = $rolesData;
    }

    protected function isInvalidResponse($response)
    {
        if ($response->isError()) {
            $this->flashSession->error($response->getErrorMessage());
            return true;
        }

        return false;
    }

    protected function isInvalidSystem($systemId)
    {
        $this->systemData = System::findFirst($systemId);

        if (! $this->systemData && $systemId) {
            $this->flashSession->error('Sistema inválido');
            return true;
        }

        return false;
    }

} 