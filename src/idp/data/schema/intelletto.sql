--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.4
-- Dumped by pg_dump version 9.3.4
-- Started on 2014-04-23 21:22:14 BRT

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 188 (class 3079 OID 11791)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2073 (class 0 OID 0)
-- Dependencies: 188
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 171 (class 1259 OID 16400)
-- Name: alteracoes_senhas; Type: TABLE; Schema: public; Owner: intelletto; Tablespace: 
--

CREATE TABLE alteracoes_senhas (
    id integer NOT NULL,
    usuario_id integer NOT NULL,
    ip character varying(15) NOT NULL,
    navegador character varying(120) NOT NULL,
    criado_em integer NOT NULL
);


ALTER TABLE public.alteracoes_senhas OWNER TO intelletto;

--
-- TOC entry 170 (class 1259 OID 16398)
-- Name: alteracoes_senhas_id_seq; Type: SEQUENCE; Schema: public; Owner: intelletto
--

CREATE SEQUENCE alteracoes_senhas_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.alteracoes_senhas_id_seq OWNER TO intelletto;

--
-- TOC entry 2074 (class 0 OID 0)
-- Dependencies: 170
-- Name: alteracoes_senhas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: intelletto
--

ALTER SEQUENCE alteracoes_senhas_id_seq OWNED BY alteracoes_senhas.id;


--
-- TOC entry 183 (class 1259 OID 24590)
-- Name: emails_confirmacoes; Type: TABLE; Schema: public; Owner: intelletto; Tablespace: 
--

CREATE TABLE emails_confirmacoes (
    id integer NOT NULL,
    usuario_id integer NOT NULL,
    codigo character varying(35) NOT NULL,
    criado_em integer NOT NULL,
    modificado_em integer,
    confirmado character(1) DEFAULT 'N'::bpchar
);


ALTER TABLE public.emails_confirmacoes OWNER TO intelletto;

--
-- TOC entry 182 (class 1259 OID 24588)
-- Name: emails_confirmacoes_id_seq; Type: SEQUENCE; Schema: public; Owner: intelletto
--

CREATE SEQUENCE emails_confirmacoes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.emails_confirmacoes_id_seq OWNER TO intelletto;

--
-- TOC entry 2075 (class 0 OID 0)
-- Dependencies: 182
-- Name: emails_confirmacoes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: intelletto
--

ALTER SEQUENCE emails_confirmacoes_id_seq OWNED BY emails_confirmacoes.id;


--
-- TOC entry 187 (class 1259 OID 24607)
-- Name: logins_efetuados; Type: TABLE; Schema: public; Owner: intelletto; Tablespace: 
--

CREATE TABLE logins_efetuados (
    id integer NOT NULL,
    usuario_id integer NOT NULL,
    ip character varying(15) NOT NULL,
    navegador character varying(120) NOT NULL
);


ALTER TABLE public.logins_efetuados OWNER TO intelletto;

--
-- TOC entry 186 (class 1259 OID 24605)
-- Name: logins_efetuados_id_seq; Type: SEQUENCE; Schema: public; Owner: intelletto
--

CREATE SEQUENCE logins_efetuados_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.logins_efetuados_id_seq OWNER TO intelletto;

--
-- TOC entry 2076 (class 0 OID 0)
-- Dependencies: 186
-- Name: logins_efetuados_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: intelletto
--

ALTER SEQUENCE logins_efetuados_id_seq OWNED BY logins_efetuados.id;


--
-- TOC entry 185 (class 1259 OID 24599)
-- Name: logins_falhos; Type: TABLE; Schema: public; Owner: intelletto; Tablespace: 
--

CREATE TABLE logins_falhos (
    id integer NOT NULL,
    usuario_id integer NOT NULL,
    ip character varying(15) NOT NULL,
    tentativa integer NOT NULL
);


ALTER TABLE public.logins_falhos OWNER TO intelletto;

--
-- TOC entry 184 (class 1259 OID 24597)
-- Name: logins_falhos_id_seq; Type: SEQUENCE; Schema: public; Owner: intelletto
--

CREATE SEQUENCE logins_falhos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.logins_falhos_id_seq OWNER TO intelletto;

--
-- TOC entry 2077 (class 0 OID 0)
-- Dependencies: 184
-- Name: logins_falhos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: intelletto
--

ALTER SEQUENCE logins_falhos_id_seq OWNED BY logins_falhos.id;


--
-- TOC entry 173 (class 1259 OID 16408)
-- Name: logins_tokens; Type: TABLE; Schema: public; Owner: intelletto; Tablespace: 
--

CREATE TABLE logins_tokens (
    id integer NOT NULL,
    usuario_id integer NOT NULL,
    token character varying(45) NOT NULL,
    navegador character varying(120) NOT NULL,
    criado_em integer NOT NULL
);


ALTER TABLE public.logins_tokens OWNER TO intelletto;

--
-- TOC entry 172 (class 1259 OID 16406)
-- Name: logins_tokens_id_seq; Type: SEQUENCE; Schema: public; Owner: intelletto
--

CREATE SEQUENCE logins_tokens_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.logins_tokens_id_seq OWNER TO intelletto;

--
-- TOC entry 2078 (class 0 OID 0)
-- Dependencies: 172
-- Name: logins_tokens_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: intelletto
--

ALTER SEQUENCE logins_tokens_id_seq OWNED BY logins_tokens.id;


--
-- TOC entry 175 (class 1259 OID 16416)
-- Name: perfis; Type: TABLE; Schema: public; Owner: intelletto; Tablespace: 
--

CREATE TABLE perfis (
    id integer NOT NULL,
    nome character varying(70) NOT NULL,
    ativo character(1) NOT NULL
);


ALTER TABLE public.perfis OWNER TO intelletto;

--
-- TOC entry 174 (class 1259 OID 16414)
-- Name: perfis_id_seq; Type: SEQUENCE; Schema: public; Owner: intelletto
--

CREATE SEQUENCE perfis_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.perfis_id_seq OWNER TO intelletto;

--
-- TOC entry 2079 (class 0 OID 0)
-- Dependencies: 174
-- Name: perfis_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: intelletto
--

ALTER SEQUENCE perfis_id_seq OWNED BY perfis.id;


--
-- TOC entry 177 (class 1259 OID 16424)
-- Name: permissoes; Type: TABLE; Schema: public; Owner: intelletto; Tablespace: 
--

CREATE TABLE permissoes (
    id integer NOT NULL,
    perfil_id integer NOT NULL,
    recurso character varying(45) NOT NULL,
    acao character varying(45) NOT NULL
);


ALTER TABLE public.permissoes OWNER TO intelletto;

--
-- TOC entry 176 (class 1259 OID 16422)
-- Name: permissoes_id_seq; Type: SEQUENCE; Schema: public; Owner: intelletto
--

CREATE SEQUENCE permissoes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.permissoes_id_seq OWNER TO intelletto;

--
-- TOC entry 2080 (class 0 OID 0)
-- Dependencies: 176
-- Name: permissoes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: intelletto
--

ALTER SEQUENCE permissoes_id_seq OWNED BY permissoes.id;


--
-- TOC entry 179 (class 1259 OID 16432)
-- Name: resets_senhas; Type: TABLE; Schema: public; Owner: intelletto; Tablespace: 
--

CREATE TABLE resets_senhas (
    id integer NOT NULL,
    usuario_id integer NOT NULL,
    codigo character varying(50) NOT NULL,
    criado_em integer NOT NULL,
    modificado_em integer,
    reset character(1) NOT NULL
);


ALTER TABLE public.resets_senhas OWNER TO intelletto;

--
-- TOC entry 178 (class 1259 OID 16430)
-- Name: resets_senhas_id_seq; Type: SEQUENCE; Schema: public; Owner: intelletto
--

CREATE SEQUENCE resets_senhas_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.resets_senhas_id_seq OWNER TO intelletto;

--
-- TOC entry 2081 (class 0 OID 0)
-- Dependencies: 178
-- Name: resets_senhas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: intelletto
--

ALTER SEQUENCE resets_senhas_id_seq OWNED BY resets_senhas.id;


--
-- TOC entry 181 (class 1259 OID 16440)
-- Name: usuarios; Type: TABLE; Schema: public; Owner: intelletto; Tablespace: 
--

CREATE TABLE usuarios (
    id integer NOT NULL,
    perfil_id integer NOT NULL,
    nome character varying(100) NOT NULL,
    email character varying(100) NOT NULL,
    senha character varying(60) NOT NULL,
    mudar_senha character(1) DEFAULT NULL::bpchar,
    banido character(1) NOT NULL,
    suspenso character(1) NOT NULL,
    ativo character(1) DEFAULT NULL::bpchar
);


ALTER TABLE public.usuarios OWNER TO intelletto;

--
-- TOC entry 180 (class 1259 OID 16438)
-- Name: usuarios_id_seq; Type: SEQUENCE; Schema: public; Owner: intelletto
--

CREATE SEQUENCE usuarios_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usuarios_id_seq OWNER TO intelletto;

--
-- TOC entry 2082 (class 0 OID 0)
-- Dependencies: 180
-- Name: usuarios_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: intelletto
--

ALTER SEQUENCE usuarios_id_seq OWNED BY usuarios.id;


--
-- TOC entry 1911 (class 2604 OID 16403)
-- Name: id; Type: DEFAULT; Schema: public; Owner: intelletto
--

ALTER TABLE ONLY alteracoes_senhas ALTER COLUMN id SET DEFAULT nextval('alteracoes_senhas_id_seq'::regclass);


--
-- TOC entry 1919 (class 2604 OID 24593)
-- Name: id; Type: DEFAULT; Schema: public; Owner: intelletto
--

ALTER TABLE ONLY emails_confirmacoes ALTER COLUMN id SET DEFAULT nextval('emails_confirmacoes_id_seq'::regclass);


--
-- TOC entry 1922 (class 2604 OID 24610)
-- Name: id; Type: DEFAULT; Schema: public; Owner: intelletto
--

ALTER TABLE ONLY logins_efetuados ALTER COLUMN id SET DEFAULT nextval('logins_efetuados_id_seq'::regclass);


--
-- TOC entry 1921 (class 2604 OID 24602)
-- Name: id; Type: DEFAULT; Schema: public; Owner: intelletto
--

ALTER TABLE ONLY logins_falhos ALTER COLUMN id SET DEFAULT nextval('logins_falhos_id_seq'::regclass);


--
-- TOC entry 1912 (class 2604 OID 16411)
-- Name: id; Type: DEFAULT; Schema: public; Owner: intelletto
--

ALTER TABLE ONLY logins_tokens ALTER COLUMN id SET DEFAULT nextval('logins_tokens_id_seq'::regclass);


--
-- TOC entry 1913 (class 2604 OID 16419)
-- Name: id; Type: DEFAULT; Schema: public; Owner: intelletto
--

ALTER TABLE ONLY perfis ALTER COLUMN id SET DEFAULT nextval('perfis_id_seq'::regclass);


--
-- TOC entry 1914 (class 2604 OID 16427)
-- Name: id; Type: DEFAULT; Schema: public; Owner: intelletto
--

ALTER TABLE ONLY permissoes ALTER COLUMN id SET DEFAULT nextval('permissoes_id_seq'::regclass);


--
-- TOC entry 1915 (class 2604 OID 16435)
-- Name: id; Type: DEFAULT; Schema: public; Owner: intelletto
--

ALTER TABLE ONLY resets_senhas ALTER COLUMN id SET DEFAULT nextval('resets_senhas_id_seq'::regclass);


--
-- TOC entry 1916 (class 2604 OID 16443)
-- Name: id; Type: DEFAULT; Schema: public; Owner: intelletto
--

ALTER TABLE ONLY usuarios ALTER COLUMN id SET DEFAULT nextval('usuarios_id_seq'::regclass);


--
-- TOC entry 2049 (class 0 OID 16400)
-- Dependencies: 171
-- Data for Name: alteracoes_senhas; Type: TABLE DATA; Schema: public; Owner: intelletto
--

COPY alteracoes_senhas (id, usuario_id, ip, navegador, criado_em) FROM stdin;
\.


--
-- TOC entry 2083 (class 0 OID 0)
-- Dependencies: 170
-- Name: alteracoes_senhas_id_seq; Type: SEQUENCE SET; Schema: public; Owner: intelletto
--

SELECT pg_catalog.setval('alteracoes_senhas_id_seq', 1, false);


--
-- TOC entry 2061 (class 0 OID 24590)
-- Dependencies: 183
-- Data for Name: emails_confirmacoes; Type: TABLE DATA; Schema: public; Owner: intelletto
--

COPY emails_confirmacoes (id, usuario_id, codigo, criado_em, modificado_em, confirmado) FROM stdin;
\.


--
-- TOC entry 2084 (class 0 OID 0)
-- Dependencies: 182
-- Name: emails_confirmacoes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: intelletto
--

SELECT pg_catalog.setval('emails_confirmacoes_id_seq', 1, false);


--
-- TOC entry 2065 (class 0 OID 24607)
-- Dependencies: 187
-- Data for Name: logins_efetuados; Type: TABLE DATA; Schema: public; Owner: intelletto
--

COPY logins_efetuados (id, usuario_id, ip, navegador) FROM stdin;
1	1	127.0.0.1	Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:28.0) Gecko/20100101 Firefox/28.0
2	1	127.0.0.1	Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:28.0) Gecko/20100101 Firefox/28.0
3	1	127.0.0.1	Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:28.0) Gecko/20100101 Firefox/28.0
4	1	127.0.0.1	Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:28.0) Gecko/20100101 Firefox/28.0
5	1	127.0.0.1	Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:28.0) Gecko/20100101 Firefox/28.0
6	1	127.0.0.1	Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:28.0) Gecko/20100101 Firefox/28.0
7	1	127.0.0.1	Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:28.0) Gecko/20100101 Firefox/28.0
8	1	127.0.0.1	Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:28.0) Gecko/20100101 Firefox/28.0
\.


--
-- TOC entry 2085 (class 0 OID 0)
-- Dependencies: 186
-- Name: logins_efetuados_id_seq; Type: SEQUENCE SET; Schema: public; Owner: intelletto
--

SELECT pg_catalog.setval('logins_efetuados_id_seq', 8, true);


--
-- TOC entry 2063 (class 0 OID 24599)
-- Dependencies: 185
-- Data for Name: logins_falhos; Type: TABLE DATA; Schema: public; Owner: intelletto
--

COPY logins_falhos (id, usuario_id, ip, tentativa) FROM stdin;
1	1	127.0.0.1	1398039061
2	1	127.0.0.1	1398039176
3	1	127.0.0.1	1398039237
4	1	127.0.0.1	1398041389
\.


--
-- TOC entry 2086 (class 0 OID 0)
-- Dependencies: 184
-- Name: logins_falhos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: intelletto
--

SELECT pg_catalog.setval('logins_falhos_id_seq', 4, true);


--
-- TOC entry 2051 (class 0 OID 16408)
-- Dependencies: 173
-- Data for Name: logins_tokens; Type: TABLE DATA; Schema: public; Owner: intelletto
--

COPY logins_tokens (id, usuario_id, token, navegador, criado_em) FROM stdin;
1	1	211dc4d1673dda7d58492d1e431b01bf	Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:28.0) Gecko/20100101 Firefox/28.0	1398039727
2	1	211dc4d1673dda7d58492d1e431b01bf	Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:28.0) Gecko/20100101 Firefox/28.0	1398039854
3	1	211dc4d1673dda7d58492d1e431b01bf	Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:28.0) Gecko/20100101 Firefox/28.0	1398039885
4	1	211dc4d1673dda7d58492d1e431b01bf	Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:28.0) Gecko/20100101 Firefox/28.0	1398041397
\.


--
-- TOC entry 2087 (class 0 OID 0)
-- Dependencies: 172
-- Name: logins_tokens_id_seq; Type: SEQUENCE SET; Schema: public; Owner: intelletto
--

SELECT pg_catalog.setval('logins_tokens_id_seq', 4, true);


--
-- TOC entry 2053 (class 0 OID 16416)
-- Dependencies: 175
-- Data for Name: perfis; Type: TABLE DATA; Schema: public; Owner: intelletto
--

COPY perfis (id, nome, ativo) FROM stdin;
1	Administrador	S
2	Usuário	S
\.


--
-- TOC entry 2088 (class 0 OID 0)
-- Dependencies: 174
-- Name: perfis_id_seq; Type: SEQUENCE SET; Schema: public; Owner: intelletto
--

SELECT pg_catalog.setval('perfis_id_seq', 1, false);


--
-- TOC entry 2055 (class 0 OID 16424)
-- Dependencies: 177
-- Data for Name: permissoes; Type: TABLE DATA; Schema: public; Owner: intelletto
--

COPY permissoes (id, perfil_id, recurso, acao) FROM stdin;
1	1	index	index
2	1	usuario	alterarSenha
\.


--
-- TOC entry 2089 (class 0 OID 0)
-- Dependencies: 176
-- Name: permissoes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: intelletto
--

SELECT pg_catalog.setval('permissoes_id_seq', 1, false);


--
-- TOC entry 2057 (class 0 OID 16432)
-- Dependencies: 179
-- Data for Name: resets_senhas; Type: TABLE DATA; Schema: public; Owner: intelletto
--

COPY resets_senhas (id, usuario_id, codigo, criado_em, modificado_em, reset) FROM stdin;
\.


--
-- TOC entry 2090 (class 0 OID 0)
-- Dependencies: 178
-- Name: resets_senhas_id_seq; Type: SEQUENCE SET; Schema: public; Owner: intelletto
--

SELECT pg_catalog.setval('resets_senhas_id_seq', 1, false);


--
-- TOC entry 2059 (class 0 OID 16440)
-- Dependencies: 181
-- Data for Name: usuarios; Type: TABLE DATA; Schema: public; Owner: intelletto
--

COPY usuarios (id, perfil_id, nome, email, senha, mudar_senha, banido, suspenso, ativo) FROM stdin;
1	1	Marcio Paiva Barbosa	mpaivabarbosa@gmail.com	$2a$08$gulspGmmJvNIBNXS9Pi3rOGC0fprSLrIjcK1y9XMsA77yMDU9HDba	N	N	N	S
\.


--
-- TOC entry 2091 (class 0 OID 0)
-- Dependencies: 180
-- Name: usuarios_id_seq; Type: SEQUENCE SET; Schema: public; Owner: intelletto
--

SELECT pg_catalog.setval('usuarios_id_seq', 1, false);


--
-- TOC entry 1924 (class 2606 OID 16405)
-- Name: alteracoes_senhas_pkey; Type: CONSTRAINT; Schema: public; Owner: intelletto; Tablespace: 
--

ALTER TABLE ONLY alteracoes_senhas
    ADD CONSTRAINT alteracoes_senhas_pkey PRIMARY KEY (id, usuario_id);


--
-- TOC entry 1936 (class 2606 OID 24596)
-- Name: emails_confirmacoes_pkey; Type: CONSTRAINT; Schema: public; Owner: intelletto; Tablespace: 
--

ALTER TABLE ONLY emails_confirmacoes
    ADD CONSTRAINT emails_confirmacoes_pkey PRIMARY KEY (id, usuario_id);


--
-- TOC entry 1940 (class 2606 OID 24612)
-- Name: logins_efetuados_pkey; Type: CONSTRAINT; Schema: public; Owner: intelletto; Tablespace: 
--

ALTER TABLE ONLY logins_efetuados
    ADD CONSTRAINT logins_efetuados_pkey PRIMARY KEY (id, usuario_id);


--
-- TOC entry 1938 (class 2606 OID 24604)
-- Name: logins_falhos_pkey; Type: CONSTRAINT; Schema: public; Owner: intelletto; Tablespace: 
--

ALTER TABLE ONLY logins_falhos
    ADD CONSTRAINT logins_falhos_pkey PRIMARY KEY (id, usuario_id);


--
-- TOC entry 1926 (class 2606 OID 16413)
-- Name: logins_tokens_pkey; Type: CONSTRAINT; Schema: public; Owner: intelletto; Tablespace: 
--

ALTER TABLE ONLY logins_tokens
    ADD CONSTRAINT logins_tokens_pkey PRIMARY KEY (id, usuario_id);


--
-- TOC entry 1928 (class 2606 OID 16421)
-- Name: perfis_pkey; Type: CONSTRAINT; Schema: public; Owner: intelletto; Tablespace: 
--

ALTER TABLE ONLY perfis
    ADD CONSTRAINT perfis_pkey PRIMARY KEY (id);


--
-- TOC entry 1930 (class 2606 OID 16429)
-- Name: permissoes_pkey; Type: CONSTRAINT; Schema: public; Owner: intelletto; Tablespace: 
--

ALTER TABLE ONLY permissoes
    ADD CONSTRAINT permissoes_pkey PRIMARY KEY (id, perfil_id);


--
-- TOC entry 1932 (class 2606 OID 16437)
-- Name: resets_senhas_pkey; Type: CONSTRAINT; Schema: public; Owner: intelletto; Tablespace: 
--

ALTER TABLE ONLY resets_senhas
    ADD CONSTRAINT resets_senhas_pkey PRIMARY KEY (id, usuario_id);


--
-- TOC entry 1934 (class 2606 OID 16447)
-- Name: usuarios_pkey; Type: CONSTRAINT; Schema: public; Owner: intelletto; Tablespace: 
--

ALTER TABLE ONLY usuarios
    ADD CONSTRAINT usuarios_pkey PRIMARY KEY (id, perfil_id);


--
-- TOC entry 2072 (class 0 OID 0)
-- Dependencies: 5
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2014-04-23 21:22:14 BRT

--
-- PostgreSQL database dump complete
--

