$('.ui.form').form({
    identity: {
        identifier  : 'identity',
        rules: [{
            type   : 'empty',
            prompt : 'Campo de preencimento obrigatório'
        }]
    },
    secret: {
        identifier  : 'secret',
        rules: [{
            type   : 'empty',
            prompt : 'Campo de preencimento obrigatório'
        }]
    }
});