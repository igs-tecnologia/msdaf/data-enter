<?php

namespace Account\Controller;

use Account\Model\User as ModelUser;

class IndexController extends \Intelletto\Webservice\Mvc\Controller
{
    public function indexAction()
    {
        $id = $this->session->get('identity')['userId'];
        $user = ModelUser::findFirstById($id);
        
        $this->view->data = $user;
    }
}