<?php

namespace Account\Controller;

use Account\Model\User;

class PasswordController extends ControllerAbstract
{
    public function changeAction()
    {
        $this->view->userId = $this->session->get('identity')['userId'];
    }
    
    public function saveAction()
    {
        $userId=$this->session->get('identity')['userId'];
        $data = $this->request->getPost();
        $data['secret'] = password_hash($data['password'], CRYPT_BLOWFISH);

        unset($data['password']);
        unset($data['passwordConf']);

        $user = User::findFirstById($userId);

        if (! $user->save($data)) {
            $this->flashSession->error($user->getMessages());
            return $this->response->redirect('account/password/change');
        }

        $this->flashSession->success('Alteração realizada com sucesso!');
        return $this->response->redirect('account');
    }
}