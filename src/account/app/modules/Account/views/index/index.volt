<div class="thirteen wide column">
    <h3 class="body-title-page" data-menu=".usuarios" style="margin-bottom: 30px;">
        Meu perfil
    </h3>
    <div class="ui">
        <form class="ui form segment form-bg">
            <div class="ui error message"></div>
            <input name="id" value="{{ data.id }}" type="hidden">
            <div class="field">
                <label>Nome Completo</label>
                <div class="ui fluid labeled input">
                    <input id='name' name="name" value="{{ data.name }}" readonly="readonly" type="text">
                    <div class="ui corner label">
                        <i class="icon asterisk"></i>
                    </div>                            
                </div>
            </div>
            <div class="two fields">
                <div class="field">
                    <label>Usuário</label>
                    <div class="ui left labeled input">
                        <input id='identity' name="identity" value="{{ data.identity }}" readonly="readonly" type="text">
                        <div class="ui corner label">
                            <i class="icon asterisk"></i>
                        </div>
                    </div>
                </div>
                <div class="field">
                    <label>E-mail</label>
                    <div class="ui left labeled input">
                        <input id="email" name="email" value="{{ data.email }}" readonly="readonly" type="text">
                        <div class="ui corner label">
                            <i class="icon asterisk"></i>
                        </div>
                    </div>
                </div>
            </div>
		</div>
    	<div class="ui segment basic floated right" style="margin:0;margin-top:30px;padding:0">
        	<!--input type="submit" class="ui submit button" value="Salvar"-->
        	<button type="submit" class="ui icon button"><i class="save icon"></i> Salvar</button>
    	</div>
     </form>
</div>