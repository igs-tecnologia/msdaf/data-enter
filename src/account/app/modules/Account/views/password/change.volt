<div class="thirteen wide column">
    <h3 class="body-title-page" data-menu=".acesso" style="margin-bottom: 30px;">
        Alterar senha
    </h3>
    <div class="ui">
        <form action="account/password/save" method="post" class="ui form segment form-bg">
            <div class="ui error message"></div>
            <div class="two fields">
                <div class="field">
                    <label>Nova Senha</label>
                    <div class="ui fluid labeled input">
                        <input id='password' name="password" value="" type="password">
                        <div class="ui corner label">
                            <i class="icon asterisk"></i>
                        </div>                            
                    </div>
                </div>
                <div class="field">
                    <label>Confirmar Senha</label>
                    <div class="ui fluid labeled input">
                        <input id='passwordConf' name="passwordConf" value="" type="password">
                        <div class="ui corner label">
                            <i class="icon asterisk"></i>
                        </div>                            
                    </div>
                </div>
            </div>
    	</div>
    	<div class="ui segment basic floated right" style="margin:0;margin-top:30px;padding:0">
        	<!--input type="submit" class="ui submit button" value="Salvar"-->
        	<button type="submit" class="ui icon button"><i class="save icon"></i> Salvar</button>
    	</div>
   </form>		
</div>

<script type="text/javascript">
    $(function () {
        $('.acesso').addClass('active');
    });

    $('.ui.form')
            .form({
                password: {
                    identifier: 'password',
                    rules: [{
                            type: 'empty',
                            prompt: 'Campo de preencimento obrigatório'
                        }, {
                            type: 'length[6]',
                            prompt: 'Senha de conter no mínimo 6 caracteres'
                        }]
                },
                passwordConf: {
                    identifier: 'passwordConf',
                    rules: [{
                            type: 'match[password]',
                            prompt: 'Senhas devem ser idênticas'
                        }]
                }                
            });
</script>