<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>{{appname}}</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <base href="{{ url() }}">
        
        <link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=Source+Sans+Pro:400,700|Open+Sans:300italic,400,300,700'>
        {{ assets.outputCss('cssHeader') }}
        {{ assets.outputJs('jsHeader') }}
        
        <script type="text/javascript" src="js/plugins.js"></script>
        <script type="text/javascript" src="js/main.js"></script>        
		<style type="text/css">

		.page-margin.admin {
    		margin-top: 19px;
		}

		.ui.segment.header-adm {
    		background:#f7f7f7;
    		border-bottom:1px solid #ddd;
    		padding: 7px 1em;
		}

		.ui.segment h3.ui.header.header-title {
    		font-weight:normal;
    		margin: 3px 0 0 0;
			padding: 5px 0px 2px 10px;
		}
		
		</style>
    </head>
    <body>

        <div class="ui main fixed menu">
            <div class="container">
                <div class="title item">
                    {{ logo }}
                </div>

                <!-- MENU USUÁRIO -->
                <div id="dropdown-config-account" class="ui large top right right floated pointing dropdown item current user">
                    <div class="text user-logado">{{ userName }}</div>
                    <i class="triangle down large icon"></i>
                    <div class="ui menu user-config">
                        <div class="text item ultimo-acesso">
                            <small>
                                Último acesso:
                                <em>{{ lastAccess }}</em>
                            </small>
                        </div>
                        {% for system in listSystems %}
                        {% if system['id'] == 5 %}
                            <div class="item"><i class="user icon"></i><a href="{{ redirect_system }}idp/login/index/{{system['id']}}" style="text-decoration:none">Configurações da conta</a></div>
                        {% endif %}
                        {% endfor %}
                        <div class="header item"><i class="key icon"></i>Perfis</div>
                        <div class="lista-perfis">
                            {% for role in listRoles %}
                            <a href="account/index/changeProfile/{{ role['id'] }}" title="{{ role['id'] ==  roleId ? 'Perfil atual' : '' }}" class="{{ role['id'] ==  roleId ? 'current-profile' : '' }}">{{role['name']}}</a>
                            {% endfor %}
                        </div>manager
                        <div class="item actions">
                            <a class="ui mini button right floated" href="account/index/logout">Sair</a>
                        </div>
                    </div>
                </div>

                <!-- MENU MÓDULOS -->
                <div class="ui large top right icon right floated pointing dropdown button modulos">
                    <i class="setting icon"></i>
                    <div class="ui menu lista-modulos">
                        {% for system in listSystems %}
                            {% if system['id'] == 1 %}
                            <div class="item"><i class="tasks icon"></i><a href="{{ redirect_system }}idp/login/index/{{system['id']}}" style="text-decoration:none">{{system['name']}}</a></div>
                            {% elseif system['id'] == 2 %}
                            <div class="item"><i class="dashboard icon"></i><a href="{{ redirect_system }}idp/login/index/{{system['id']}}" style="text-decoration:none">Sala de situação</a></div>
                            {% elseif system['id'] == 3 %}
                            <div class="item"><i class="tasks icon"></i><a href="{{ redirect_system }}idp/login/index/{{system['id']}}" style="text-decoration:none">Administração</a></div>
                            {% elseif system['id'] == 4 %}
                            <div class="item"><i class="puzzle piece icon"></i><a href="{{ redirect_system }}idp/login/index/{{system['id']}}" style="text-decoration:none">Gestão de dados</a></div>
                            {% elseif system['id'] == 7 %}
                            <div class="item"><i class="recursos-hidricos icon"></i><a href="{{ redirect_system }}idp/login/index/{{system['id']}}" style="text-decoration:none">Monitoramento Hidrológico</a></div>
                            {% endif %}
                        {% endfor %}
                    </div>
                </div>
            </div>
        </div>

		<div class="ui layout grid admin page-margin">
			<div class="ui segment basic header-adm">
    			<h3 class="ui header left floated header-title">Configurações da conta</h3>
			</div>
    		<div class="column wide sixteen" style="margin:0;margin-top:15px">
       	 		{{ flashSession.output() }}
    		</div>
            <div class="three wide column">
                <div class="ui small vertical fluid pointing admin menu">
                    <a class="item usuarios" href="account/index/">
                        <i class="user icon"></i> Meu Perfil
                    </a>
                    <a class="item acesso" href="account/password/change">
                        <i class="key icon"></i> Alterar Senha
                    </a>
                </div>
            </div>
            {{ content() }}
		</div>

            

    </body>
</html>