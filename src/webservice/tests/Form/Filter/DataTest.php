<?php

namespace Webservice\Form\Filter;

use Webservice\Form\Filter\Data;
use PHPUnit_Framework_TestCase as TestCase;

class DataTest extends TestCase
{
    protected $filter;
    
    protected function setUp()
    {
        $struct = [
            (object) ['nativeType' => 'varchar','nativeName' => 'nomevar', 'required' => true],
            (object) ['nativeType' => 'numeric','nativeName' => 'nomenum', 'required' => true],
            (object) ['nativeType' => 'integer','nativeName' => 'nomeint', 'required' => true],
            (object) ['nativeType' => 'timestamp','nativeName' => 'nometime', 'required' => true]
        ];
        
        $this->filter = new Data($struct);
    }
    
    public function testBuilder()
    {
        $this->filter->build();
        $filters = $this->filter->getFilters();
        
        $this->assertTrue(isset($filters['nomevar']));
        $this->assertTrue(isset($filters['nomenum']));
        $this->assertTrue(isset($filters['nomeint']));
        $this->assertTrue(isset($filters['nometime']));
    }
    
    /**
     * @dataProvider validData
     */
    public function testFilterValidData($data)
    {
        $this->assertTrue($this->filter->isValid($data));
    }
    
    /**
     * @dataProvider invalidData
     */
    public function testFilterInvalidData($data)
    {
        $this->assertFalse($this->filter->isValid($data));
    }
    
    public function validData()
    {
        return [
            [['nomevar' => 'a', 'nomenum' => '12,34', 'nomeint' => '1', 'nometime' => '20/10/2010']],
            [['nomevar' => 'a', 'nomenum' => '12,34', 'nomeint' => '1', 'nometime' => '20/10/2010 10:30:00']],
            [['nomevar' => 'a', 'nomenum' => '12,34', 'nomeint' => '1', 'nometime' => '1412013820']],
            [['nomevar' => 'a', 'nomenum' => '12', 'nomeint' => '1', 'nometime' => '20/10/2010']],
            [['nomevar' => 'a', 'nomenum' => '12', 'nomeint' => 1, 'nometime' => '20/10/2010']],
            [['nomevar' => 'a', 'nomenum' => 12, 'nomeint' => 1, 'nometime' => '20/10/2010']],
            [['nomevar' => ['a', 'b'], 'nomenum' => 12, 'nomeint' => 1, 'nometime' => '20/10/2010']]
        ];
    }
    
    public function invalidData()
    {
        return [
            [['nomevar' => '', 'nomenum' => '12,35', 'nomeint' => '1', 'nometime' => '20/10/2010']],
            [['nomevar' => 'a', 'nomenum' => '', 'nomeint' => '1', 'nometime' => '20/10/2010']],
            [['nomevar' => 'a', 'nomenum' => '12,35', 'nomeint' => '0', 'nometime' => '20/10/2010']],
            [['nomevar' => 'a', 'nomenum' => '12,35', 'nomeint' => '1', 'nometime' => '']],
            [['nomevar' => 'a', 'nomenum' => 'a', 'nomeint' => 'a', 'nometime' => 'a']],
            [['nomevar' => 'a', 'nomenum' => '0', 'nomeint' => '0', 'nometime' => '20/10/2010']],
            [['nomevar' => 'a', 'nomenum' => '12,35', 'nomeint' => '1', 'nometime' => '10/20/2010']],
            [['nomevar' => '0', 'nomenum' => '12,35', 'nomeint' => '1', 'nometime' => '20/10/2010']],
            [['nomevar' => 'a', 'nomenum' => 12.35, 'nomeint' => '1', 'nometime' => '20/10/2010']],
            [['nomevar' => 'a', 'nomenum' => '12,35', 'nomeint' => '1', 'nometime' => '419846328461892368743']],
        ];
    }
    
    protected function tearDown()
    {
        
    }
}