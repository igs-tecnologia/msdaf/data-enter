<?php
return [
    10 => 'Realizado com sucesso.',
    11 => 'Operação não realizada. Verifique os dados e tente novamente.',
    
    21 => 'Usuário ou senha inválido',
    22 => 'Quantidade de tentativas excedido.',
];