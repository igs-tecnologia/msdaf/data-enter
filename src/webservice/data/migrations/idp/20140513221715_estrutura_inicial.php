<?php

use Phinx\Migration\AbstractMigration;

class EstruturaInicial extends AbstractMigration {

    /**
     * Migrate Up.
     */
    public function up() {
        $this->CreateUser();
        $this->CreateUserRole();
        $this->CreateResource();
        $this->CreateRoleResource();
        $this->CreateRole();
        $this->CreateSystem();
        $this->CreateLogin();
        $this->CreateLoginFail();
        $this->CreateLoginReset();
        $this->CreateLoginToken();
        $this->CreateRegistration();
    }

    /**
     * Migrate Down.
     */
    public function down() {
        $this->dropTable('user');
        $this->dropTable('user_role');
        $this->dropTable('resource');
        $this->dropTable('role_resource');
        $this->dropTable('role');
        $this->dropTable('system');
        $this->dropTable('login');
        $this->dropTable('login_fail');
        $this->dropTable('login_reset');
        $this->dropTable('login_token');
        $this->dropTable('registration');
    }

    private function CreateUser() {
        $table = $this->table('user');
        $table->addColumn('name', 'string', array('limit' => 255))
              ->addColumn('identity', 'string', array('limit' => 255))
              ->addColumn('secret', 'string', array('limit' => 255))
              ->addColumn('email', 'string', array('limit' => 255))
              ->addIndex(array('email', 'identity'), array('unique' => true))
              ->addIndex(array('identity'), array('unique' => true))
              ->save();
    }
    
    private function CreateUserRole(){
        $table = $this->table('user_role', array('id' => false, 'primary_key' => array('role_id', 'system_id', 'user_id')));
        $table->addColumn('role_id', 'integer')
              ->addColumn('system_id', 'integer')
              ->addColumn('user_id', 'integer')
              ->addIndex(array('user_id', 'role_id', 'system_id'), array('unique' => true))
              ->save();        
    }
    
    private function CreateResource() {
        $table = $this->table('resource');
        $table->addColumn('controller', 'string', array('limit' => 255, 'default' => null, 'null' => true))
              ->addColumn('action', 'string', array('limit' => 255, 'default' => null, 'null' => true))
              ->addColumn('description', 'string', array('limit' => 255))
              ->addColumn('route', 'string', array('limit' => 255))
              ->addColumn('method', 'string', array('limit' => 10))
              ->save();
    }

    private function CreateRoleResource(){
        $table = $this->table('role_resource', array('id' => false, 'primary_key' => array('role_id', 'resource_id', 'system_id')));
        $table->addColumn('role_id', 'integer')
              ->addColumn('resource_id', 'integer')
              ->addColumn('system_id', 'integer')
              ->addIndex(array('role_id', 'resource_id', 'system_id'), array('unique' => true))
              ->save();        
    }    

    private function CreateRole(){
        $table = $this->table('role');
        $table->addColumn('name', 'string', array('limit' => 100))
              ->save();        
    }    

    private function CreateSystem() {
        $table = $this->table('system');
        $table->addColumn('name', 'string', array('limit' => 255))
              ->addColumn('return_url', 'string', array('limit' => 255))
              ->addColumn('key', 'string', array('limit' => 40))
              ->addColumn('secret', 'string', array('limit' => 60))
              ->addIndex(array('key'), array('unique' => true))
              ->save();
    }    

    private function CreateLogin() {
        $table = $this->table('login');
        $table->addColumn('user_id', 'integer')
              ->addColumn('ip', 'string', array('limit' => 15))
              ->addColumn('user_agent', 'string', array('limit' => 255))
              ->addColumn('created', 'timestamp', array('default' => 'CURRENT_TIMESTAMP'))
              ->addIndex(array('user_id'))
              ->save();
    }   
    
    private function CreateLoginFail() {
        $table = $this->table('login_fail');
        $table->addColumn('user_id', 'integer')
              ->addColumn('ip', 'string', array('limit' => 15))
              ->addColumn('user_agent', 'string', array('limit' => 255))
              ->addColumn('secret', 'string', array('limit' => 255))   
              ->addColumn('attempt', 'integer') 
              ->addColumn('created', 'timestamp', array('default' => 'CURRENT_TIMESTAMP'))
              ->addIndex(array('user_id'))
              ->save();
    }
    
    private function CreateLoginReset() {
        $table = $this->table('login_reset');
        $table->addColumn('user_id', 'integer')
              ->addColumn('token', 'string', array('limit' => 50))
              ->addColumn('created', 'timestamp', array('default' => 'CURRENT_TIMESTAMP'))
              ->addColumn('updated', 'timestamp', array('default' => null))
              ->addIndex(array('user_id'))
              ->save();
    } 
    
    private function CreateLoginToken() {
        $table = $this->table('login_token');
        $table->addColumn('role_id', 'integer')
              ->addColumn('user_id', 'integer')
              ->addColumn('token', 'string', array('limit' => 50))
              ->addColumn('ip', 'string', array('limit' => 15))
              ->addColumn('user_agent', 'string', array('limit' => 255))
              ->addColumn('created', 'timestamp', array('default' => 'CURRENT_TIMESTAMP'))
              ->addColumn('expire', 'timestamp', array('default' => null))
              ->addIndex(array('token'), array('unique' => true))
              ->save();
    } 
    
    private function CreateRegistration() {
        $table = $this->table('registration');
        $table->addColumn('user_id', 'integer')
              ->addColumn('token', 'string', array('limit' => 50))
              ->addColumn('created', 'timestamp', array('default' => 'CURRENT_TIMESTAMP'))
              ->addColumn('confirmed', 'timestamp', array('default' => null))
              ->addIndex(array('token'), array('unique' => true))
              ->save();
    } 
}
