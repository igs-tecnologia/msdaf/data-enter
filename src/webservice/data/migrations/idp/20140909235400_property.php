<?php

use Phinx\Migration\AbstractMigration;

class Property extends AbstractMigration
{
    public function change()
    {
        $table = $this->table('property');
        $table->addColumn('key', 'string', ['limit' => 255])
              ->addColumn('value', 'json')
              ->addIndex(array('key'), array('unique' => true))
              ->save();
        
        $table = $this->table('role');
        $table->addColumn('visible', 'boolean', ['default' => true])
              ->save();
        
        $table = $this->table('system');
        $table->addColumn('visible', 'boolean', ['default' => true])
              ->save();
        
        $table = $this->table('user');
        $table->addColumn('property', 'json', ['default' => null, 'null' => true])
              ->save();
    }
}