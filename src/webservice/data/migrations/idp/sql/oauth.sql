--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: oauth_access_token_scopes; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE oauth_access_token_scopes (
    id integer NOT NULL,
    access_token character varying(255) NOT NULL,
    scope character varying(255) NOT NULL
);



--
-- Name: oauth_access_token_scopes_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE oauth_access_token_scopes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



--
-- Name: oauth_access_token_scopes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE oauth_access_token_scopes_id_seq OWNED BY oauth_access_token_scopes.id;


--
-- Name: oauth_access_tokens; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE oauth_access_tokens (
    access_token character varying(255) NOT NULL,
    session_id integer NOT NULL,
    expire_time integer NOT NULL
);

--
-- Name: oauth_auth_code_scopes; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE oauth_auth_code_scopes (
    id integer NOT NULL,
    auth_code character varying(255) NOT NULL,
    scope character varying(255) NOT NULL
);


--
-- Name: oauth_auth_code_scopes_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE oauth_auth_code_scopes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



--
-- Name: oauth_auth_code_scopes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE oauth_auth_code_scopes_id_seq OWNED BY oauth_auth_code_scopes.id;


--
-- Name: oauth_client_redirect_uris; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE oauth_client_redirect_uris (
    id integer NOT NULL,
    client_id character varying(255) NOT NULL,
    redirect_uri character varying(255) NOT NULL
);



--
-- Name: oauth_client_redirect_uris_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE oauth_client_redirect_uris_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: oauth_client_redirect_uris_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE oauth_client_redirect_uris_id_seq OWNED BY oauth_client_redirect_uris.id;


--
-- Name: oauth_refresh_tokens; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE oauth_refresh_tokens (
    refresh_token character varying(255) NOT NULL,
    expire_time integer NOT NULL,
    access_token character varying(255) NOT NULL
);



--
-- Name: oauth_scopes; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE oauth_scopes (
    id character varying(255) NOT NULL,
    description character varying(255) NOT NULL
);



--
-- Name: oauth_session_scopes; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE oauth_session_scopes (
    id integer NOT NULL,
    scope character varying(255) NOT NULL,
    session_id integer NOT NULL
);

--
-- Name: oauth_session_scopes_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE oauth_session_scopes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



--
-- Name: oauth_session_scopes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE oauth_session_scopes_id_seq OWNED BY oauth_session_scopes.id;


--
-- Name: oauth_sessions; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE oauth_sessions (
    id integer NOT NULL,
    owner_type character varying(255) NOT NULL,
    owner_id character varying(255) NOT NULL,
    client_id character varying(255) NOT NULL,
    client_redirect_uri character varying(255)
);



--
-- Name: oauth_sessions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE oauth_sessions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



--
-- Name: oauth_sessions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE oauth_sessions_id_seq OWNED BY oauth_sessions.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY oauth_access_token_scopes ALTER COLUMN id SET DEFAULT nextval('oauth_access_token_scopes_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY oauth_auth_code_scopes ALTER COLUMN id SET DEFAULT nextval('oauth_auth_code_scopes_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY oauth_client_redirect_uris ALTER COLUMN id SET DEFAULT nextval('oauth_client_redirect_uris_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY oauth_session_scopes ALTER COLUMN id SET DEFAULT nextval('oauth_session_scopes_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY oauth_sessions ALTER COLUMN id SET DEFAULT nextval('oauth_sessions_id_seq'::regclass);


--
-- Name: oauth_access_token_scopes_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY oauth_access_token_scopes
    ADD CONSTRAINT oauth_access_token_scopes_pkey PRIMARY KEY (id);


--
-- Name: oauth_access_tokens_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY oauth_access_tokens
    ADD CONSTRAINT oauth_access_tokens_pkey PRIMARY KEY (access_token);


--
-- Name: oauth_auth_code_scopes_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY oauth_auth_code_scopes
    ADD CONSTRAINT oauth_auth_code_scopes_pkey PRIMARY KEY (id);


--
-- Name: oauth_client_redirect_uris_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY oauth_client_redirect_uris
    ADD CONSTRAINT oauth_client_redirect_uris_pkey PRIMARY KEY (id);


--
-- Name: oauth_scopes_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY oauth_scopes
    ADD CONSTRAINT oauth_scopes_pkey PRIMARY KEY (id);


--
-- Name: oauth_session_scopes_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY oauth_session_scopes
    ADD CONSTRAINT oauth_session_scopes_pkey PRIMARY KEY (id);


--
-- Name: oauth_sessions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY oauth_sessions
    ADD CONSTRAINT oauth_sessions_pkey PRIMARY KEY (id);


--
-- Name: lnk_oauth_sessions_system2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY oauth_sessions
    ADD CONSTRAINT lnk_oauth_sessions_system2 FOREIGN KEY (client_id) REFERENCES system(key) MATCH FULL ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: oauth_access_token_scopes_access_token_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY oauth_access_token_scopes
    ADD CONSTRAINT oauth_access_token_scopes_access_token_foreign FOREIGN KEY (access_token) REFERENCES oauth_access_tokens(access_token) ON DELETE CASCADE;


--
-- Name: oauth_access_token_scopes_scope_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY oauth_access_token_scopes
    ADD CONSTRAINT oauth_access_token_scopes_scope_foreign FOREIGN KEY (scope) REFERENCES oauth_scopes(id) ON DELETE CASCADE;


--
-- Name: oauth_access_tokens_session_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY oauth_access_tokens
    ADD CONSTRAINT oauth_access_tokens_session_id_foreign FOREIGN KEY (session_id) REFERENCES oauth_sessions(id) ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

