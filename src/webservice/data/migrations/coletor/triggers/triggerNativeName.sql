-- Function: create_table()

-- DROP FUNCTION create_table();

-- Function: create_table()

-- DROP FUNCTION create_table();

CREATE OR REPLACE FUNCTION create_table()
  RETURNS trigger AS
$BODY$
    DECLARE
    struct_json json;
    struct_json_text record;
    fk_table RECORD;
    desc_views RECORD;
    schemaname varchar;
    tablename varchar;
    tablenameold varchar;
    fk_tablename varchar;
    tablename_type varchar;

    field_line varchar;
    field_line_plain varchar;
    field_line_convert varchar;
    elements varchar[];
    elements_plain varchar[];
    elementsconvert varchar[];
    elementscomment varchar[];
    recreateviews varchar[];
    elementsfk varchar[];
    quotename varchar;
    name_field varchar;
    fk_col varchar;
    name_type varchar;
    name_lengh varchar;
    adapter varchar;
    precision_int varchar;
    name_comment varchar;
    required varchar;
    unique_col varchar;
    null_col varchar;
    count int;
    count_views int;
    count_fk int;
    comment_str varchar;
    fk_str varchar;

    teste varchar[];
    r int;
    BEGIN
	schemaname:='data_store';
        IF (TG_OP = 'DELETE') THEN
	    tablenameold:=OLD.group_id::varchar||'_'||lower(unaccent_string(regexp_replace(OLD.name, '\W', '', 'g')));
	    EXECUTE 'DROP TABLE IF EXISTS "'||schemaname||'"."'||tablenameold||'" CASCADE;';
	    EXECUTE 'DROP TYPE IF EXISTS "'||schemaname||'"."'||tablenameold||'_type" CASCADE;';
	    RETURN OLD; -- result is ignored since this is an AFTER trigger
      ELSIF (TG_OP = 'UPDATE') THEN
	    IF ( NEW.struct::text=OLD.struct::text ) THEN
		      RETURN NEW;
	    ELSE
          tablenameold:=OLD.group_id::varchar||'_'||lower(unaccent_string(regexp_replace(OLD.name, '\W', '', 'g')));
          tablename:=NEW.group_id::varchar||'_'||lower(unaccent_string(regexp_replace(NEW.name, '\W', '', 'g')));
          count_fk:=1;
          count:=1;
          elements[count]=' id integer NOT NULL ';
          elementscomment[count]='COMMENT ON COLUMN "'||schemaname||'"."'||tablename||'".id IS ''ID'';';
          count:=count+1;
          elements[count]=' date_row timestamp NOT NULL ';
          elementscomment[count]='COMMENT ON COLUMN "'||schemaname||'"."'||tablename||'".date_row IS ''Data de atualização'';';
          count:=count+1;
          EXECUTE 'DROP TABLE IF EXISTS "'||schemaname||'"."'||tablenameold||'" CASCADE ;';
          EXECUTE 'DROP TYPE IF EXISTS "'||schemaname||'"."'||tablenameold||'_type" CASCADE;';

          IF (json_array_length(NEW.struct) != 0) THEN
            FOR struct_json IN select * from json_array_elements(NEW.struct)
              LOOP
            name_field:=json_extract_path_text(struct_json,'nativeName');
            quotename:='"';
            name_type:=json_extract_path_text(struct_json,'type');
            name_comment:=json_extract_path_text(struct_json,'name');
            required:=json_extract_path_text(struct_json,'required');
            unique_col:=json_extract_path_text(struct_json,'unique');
            adapter:=json_extract_path_text(struct_json,'adapter');


            IF (unique_col IS NOT NULL AND unique_col='1') THEN

              elementsfk[count_fk]='ALTER TABLE "'||schemaname||'"."'||tablename||'" ADD CONSTRAINT "unique_'||tablename||'_'||name_field||'" UNIQUE ("'||name_field||'")';
              count_fk:=count_fk+1;
            END IF;

            elementsconvert[count]:='popo."'||name_field||'"';
            IF (name_type='date'::varchar) THEN
              name_type:='timestamp';
            END IF;
            IF (name_type='hidden'::varchar) THEN
              name_type:='varchar';
            END IF;

            IF (name_type='text'::varchar) THEN
              name_type:='varchar';
              name_lengh:=json_extract_path_text(struct_json,'length');
              IF (name_lengh <> '') THEN
                name_type:=name_type||'('||name_lengh||')';
              END IF;
            END IF;

            IF (name_type='map'::varchar) THEN
              name_type:='geometry';
              elementsconvert[count]:='ST_MakePoint( (string_to_array( popo."'||name_field||'",'',''))[2]::double precision, (string_to_array( popo."'||name_field||'",'',''))[1]::double precision)';
            END IF;

            IF (name_type='decimal'::varchar) THEN
              name_type:='numeric';
            END IF;

            IF (name_type='number'::varchar) THEN
              name_type:='int4';
            END IF;

            IF (name_type='file'::varchar) THEN
              name_type:='varchar(5000)';
            END IF;

            IF (name_type='options'::varchar) THEN
              IF (adapter='static'::varchar) THEN
                name_type:='varchar';
              END IF;
              IF (adapter='form'::varchar) THEN
                fk_col:=json_extract_path_text(struct_json,'field');
                SELECT id,name,group_id,(SELECT element->>'nativeType'  FROM json_array_elements(struct) as element WHERE element->>'name'=(string_to_array(fk_col,'.'))[2]::varchar ) as struct  INTO fk_table FROM public.form where id=(string_to_array(fk_col,'.'))[1]::integer LIMIT 1;
                fk_tablename:=fk_table.group_id::varchar||'_'||lower(unaccent_string(regexp_replace(fk_table.name, '\W', '', 'g')));
                name_type:='integer';
                --elementsfk[count_fk]='ALTER TABLE "'||schemaname||'"."'||tablename||'" ADD CONSTRAINT "'||fk_tablename||'_fkey" FOREIGN KEY("'||name_field||'") REFERENCES '||schemaname||'."'||fk_tablename||'"(id)';
              END IF;
              IF (adapter='sisrel'::varchar) THEN
                name_type:='varchar';
              END IF;
              count_fk:=count_fk+1;
            END IF;



            --IF (required='1'::varchar AND required IS NOT NULL) THEN
            --	null_col:=' NOT NULL ';
            --ELSE
            --	null_col:=' NULL ';
            --END IF;
            null_col:=' NULL ';
            elements[count]:='"'||name_field||'" '||name_type||' '||null_col;

            IF (name_type='geometry') THEN
              elements_plain[count]:='"'||name_field||'" varchar';
            ELSE
              elements_plain[count]:='"'||name_field||'" '||name_type;
            END IF;

            elementscomment[count]:='COMMENT ON COLUMN '||schemaname||'."'||tablename||'"."'||name_field||'" IS '''||name_comment||''';';

            count:=count+1;
          END LOOP;
            field_line:=array_to_string(elements, ',');
            field_line_plain:=array_to_string(elements_plain, ',');
            field_line_convert:=array_to_string(elementsconvert, ',');
            tablename_type:=tablename||'_type';

            EXECUTE 'CREATE TABLE "'||schemaname||'"."'||tablename||'"('||field_line||');';
            EXECUTE 'CREATE TYPE "'||schemaname||'"."'||tablename_type||'" AS ('||field_line_plain||');';

            FOREACH comment_str IN ARRAY elementscomment
            LOOP
              EXECUTE comment_str;
            END LOOP;

            IF (count(elementsfk) != 0) THEN
              FOREACH fk_str IN ARRAY elementsfk
              LOOP
                EXECUTE fk_str;
              END LOOP;
            END IF;

            EXECUTE 'INSERT INTO "'||schemaname||'"."'||tablename||'" SELECT id,(CASE WHEN updated IS NOT NULL THEN updated ELSE created END) as date_row, '||field_line_convert||' FROM data as a, LATERAL(SELECT * FROM json_populate_recordset(null::"'||schemaname||'"."'||tablename_type||'", (SELECT json_agg(value) FROM data WHERE data.id = a.id AND data.deleted=0))) AS popo WHERE form_id  = '||NEW.id||';';
            EXECUTE 'ALTER TABLE "'||schemaname||'"."'||tablename||'" ADD PRIMARY KEY (id);';
          ELSE
        EXECUTE 'CREATE TABLE "'||schemaname||'"."'||tablename||'";';
          END IF;
          EXECUTE 'COMMENT ON TABLE "'||schemaname||'"."'||tablename||'" IS '''||NEW.name||''';';

          RETURN NEW; -- result is ignored since this is an AFTER trigger
      END IF;
        ELSIF (TG_OP = 'INSERT') THEN
            tablename:=NEW.group_id::varchar||'_'||lower(unaccent_string(regexp_replace(NEW.name, '\W', '', 'g')));
            count_fk:=1;
            count:=1;
            elements[count]=' id integer NOT NULL ';
            elementscomment[count]='COMMENT ON COLUMN "'||schemaname||'"."'||tablename||'".id IS ''ID'';';
            count:=count+1;
            elements[count]=' date_row timestamp NOT NULL ';
            elementscomment[count]='COMMENT ON COLUMN "'||schemaname||'"."'||tablename||'".date_row IS ''Data de atualização'';';
            count:=count+1;
		IF (json_array_length(NEW.struct) != 0) THEN
			FOR struct_json IN select * from json_array_elements(NEW.struct)
			    LOOP
				name_field:=json_extract_path_text(struct_json,'nativeName');
				quotename:='"';
				name_type:=json_extract_path_text(struct_json,'type');
				name_comment:=json_extract_path_text(struct_json,'name');
				required:=json_extract_path_text(struct_json,'required');
				unique_col:=json_extract_path_text(struct_json,'unique');
				adapter:=json_extract_path_text(struct_json,'adapter');

				IF (unique_col IS NOT NULL AND unique_col='1') THEN

					elementsfk[count_fk]='ALTER TABLE "'||schemaname||'"."'||tablename||'" ADD CONSTRAINT "unique_'||tablename||'_'||name_field||'" UNIQUE ("'||name_field||'")';
					count_fk:=count_fk+1;
				END IF;

				IF (name_type='date'::varchar) THEN
					name_type:='timestamp';
				END IF;
				IF (name_type='hidden'::varchar) THEN
					name_type:='varchar';
				END IF;

				IF (name_type='text'::varchar) THEN
					name_type:='varchar';
					name_lengh:=json_extract_path_text(struct_json,'length');
					IF (name_lengh <> '') THEN
					  name_type:=name_type||'('||name_lengh||')';
					END IF;
				END IF;

				IF (name_type='map'::varchar) THEN
					name_type:='geometry';
				END IF;

				IF (name_type='decimal'::varchar) THEN
					name_type:='numeric';
				END IF;

				IF (name_type='number'::varchar) THEN
					name_type:='int4';
				END IF;

				IF (name_type='file'::varchar) THEN
					name_type:='varchar(5000)';
				END IF;

				IF (name_type='options'::varchar) THEN
					IF (adapter='static'::varchar) THEN
						name_type:='varchar';
					END IF;
					IF (adapter='form'::varchar) THEN
						fk_col:=json_extract_path_text(struct_json,'field');
						SELECT id,name,group_id,(SELECT element->>'nativeType'  FROM json_array_elements(struct) as element WHERE element->>'name'=(string_to_array(fk_col,'.'))[2]::varchar ) as struct  INTO fk_table FROM public.form where id=(string_to_array(fk_col,'.'))[1]::integer LIMIT 1;
						fk_tablename:=fk_table.group_id::varchar||'_'||lower(unaccent_string(regexp_replace(fk_table.name, '\W', '', 'g')));
						name_type:='integer';
						--elementsfk[count_fk]='ALTER TABLE "'||schemaname||'"."'||tablename||'" ADD CONSTRAINT "'||fk_tablename||'_fkey" FOREIGN KEY("'||name_field||'") REFERENCES '||schemaname||'."'||fk_tablename||'"(id)';
					END IF;
					IF (adapter='sisrel'::varchar) THEN
						name_type:='varchar';
					END IF;
					count_fk:=count_fk+1;
				END IF;


				--
				--IF (required='1'::varchar AND required IS NOT NULL) THEN
				--	null_col:=' NOT NULL ';
				--ELSE
				--	null_col:=' NULL ';
				--END IF;
        null_col:=' NULL ';
				elements[count]:='"'||name_field||'" '||name_type||' '||null_col;
				IF (name_type='geometry') THEN
					elements_plain[count]:='"'||name_field||'" varchar';
				ELSE
					elements_plain[count]:='"'||name_field||'" '||name_type;
				END IF;
				elementscomment[count]:='COMMENT ON COLUMN "'||schemaname||'"."'||tablename||'"."'||name_field||'" IS '''||name_comment||''';';
				count:=count+1;
			END LOOP;

		    field_line:=array_to_string(elements, ',');
		    field_line_plain:=array_to_string(elements_plain, ',');
		    tablename_type:=tablename||'_type';

		    EXECUTE 'CREATE TABLE "'||schemaname||'"."'||tablename||'"('||field_line||');';
		    EXECUTE 'CREATE TYPE "'||schemaname||'"."'||tablename_type||'" AS ('||field_line_plain||');';

		    FOREACH comment_str IN ARRAY elementscomment
		    LOOP
			    EXECUTE comment_str;
		    END LOOP;
		    IF (count(elementsfk) != 0) THEN
			    FOREACH fk_str IN ARRAY elementsfk
			    LOOP
				    EXECUTE fk_str;
			    END LOOP;
		    END IF;
	    ELSE
		EXECUTE 'CREATE TABLE "'||schemaname||'"."'||tablename||'";';
            END IF;
            EXECUTE 'COMMENT ON TABLE "'||schemaname||'"."'||tablename||'" IS '''||NEW.name||''';';
            RETURN NEW; -- result is ignored since this is an AFTER trigger
        END IF;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;




  -- Function: insert_data()

-- DROP FUNCTION insert_data();

CREATE OR REPLACE FUNCTION insert_data()
  RETURNS trigger AS
$BODY$
    DECLARE
    struct_json json;
    struct_json_text record;
    schemaname varchar;
    tablename varchar;
    tablename_type varchar;
    field_line varchar;
    elements varchar[];
    elementscomment varchar[];
    elementsconvert varchar[];
    field_line_convert varchar;
    quotename varchar;
    name_field varchar;
    name_type varchar;
    name_lengh varchar;
    precision_int varchar;
    name_comment varchar;
    required varchar;
    null_col varchar;
    count int;
    countconvert int;
    r int;
    ref RECORD;
    ref2 RECORD;
    data_new RECORD;
    BEGIN
	count:=1;
	countconvert:=1;
	schemaname:='data_store';
        IF (TG_OP = 'DELETE') THEN
		SELECT * INTO ref FROM public.form where id=OLD.form_id LIMIT 1;
		tablename:=ref.group_id::varchar||'_'||lower(unaccent_string(regexp_replace(ref.name, '\W', '', 'g')));
		EXECUTE 'DELETE FROM "'||schemaname||'"."'||tablename||'" WHERE id='||OLD.id||';';
        ELSIF (TG_OP = 'UPDATE') THEN
	    SELECT * INTO ref FROM public.form where id=NEW.form_id LIMIT 1;
		FOR struct_json IN select * from json_array_elements(ref.struct)
		LOOP
			quotename:='"';
			name_field:=json_extract_path_text(struct_json,'nativeName');
			name_type:=json_extract_path_text(struct_json,'type');
			elementsconvert[countconvert]:='popo.'||name_field;
			IF (name_type='map'::varchar) THEN
				elementsconvert[countconvert]:='ST_MakePoint( (string_to_array( popo.'||name_field||','',''))[2]::double precision, (string_to_array( popo.'||name_field||','',''))[1]::double precision)';
			END IF;
			IF (name_type='number'::varchar) THEN
				elementsconvert[countconvert]:='(CASE WHEN ( popo.'||name_field||'::text ~ ''^([0-9]+[.]?[0-9]*|[.][0-9]+)$'' = true) THEN popo.'||name_field||' ELSE 0 END) as '||name_field||' ';
			END IF;
			IF (name_type='decimal'::varchar) THEN
				elementsconvert[countconvert]:='(CASE WHEN (popo.'||name_field||'::text ~ ''^([0-9]+[.]?[0-9]*|[.][0-9]+)$'' = true) THEN popo.'||name_field||' ELSE 0 END) as '||name_field||' ';
			END IF;

			countconvert:=countconvert+1;
		END LOOP;
		tablename:=ref.group_id::varchar||'_'||lower(unaccent_string(regexp_replace(ref.name, '\W', '', 'g')));
		tablename_type:=tablename||'_type';
		field_line_convert:=array_to_string(elementsconvert, ',');
		EXECUTE 'DELETE FROM "'||schemaname||'"."'||tablename||'" WHERE id  = '||NEW.id||'; ';

		EXECUTE 'INSERT INTO "'||schemaname||'"."'||tablename||'" SELECT id,(CASE WHEN updated IS NOT NULL THEN updated ELSE created END) as date_row, '||field_line_convert||' FROM data as a, LATERAL(SELECT * FROM json_populate_recordset(null::'||schemaname||'."'||tablename_type||'", (SELECT json_agg(value) FROM data WHERE data.id = a.id AND data.deleted=0))) AS popo WHERE a.id  = '||NEW.id||'; ';

        ELSIF (TG_OP = 'INSERT') THEN
		SELECT * INTO ref FROM public.form where id=NEW.form_id LIMIT 1;
		FOR struct_json IN select * from json_array_elements(ref.struct)
		LOOP
			quotename:='"';
			name_field:=struct_json->'nativeName';
			name_type:=replace((struct_json->'type'::varchar)::text,quotename::text,''::text);
			elementsconvert[countconvert]:='popo.'||name_field;
			IF (name_type='map'::varchar) THEN
				elementsconvert[countconvert]:='ST_MakePoint( (string_to_array( popo.'||name_field||','',''))[2]::double precision, (string_to_array( popo.'||name_field||','',''))[1]::double precision)';
			END IF;
			IF (name_type='number'::varchar) THEN
				elementsconvert[countconvert]:='(CASE WHEN (popo.'||name_field||'::varchar ~ ''^([0-9]+[.]?[0-9]*|[.][0-9]+)$'' = true) THEN popo.'||name_field||' ELSE 0 END) as '||name_field||' ';
			END IF;
			IF (name_type='decimal'::varchar) THEN
				elementsconvert[countconvert]:='(CASE WHEN (popo.'||name_field||'::varchar ~ ''^([0-9]+[.]?[0-9]*|[.][0-9]+)$'' = true) THEN popo.'||name_field||' ELSE 0 END) as '||name_field||' ';
			END IF;

			countconvert:=countconvert+1;
		END LOOP;
		tablename:=ref.group_id::varchar||'_'||lower(unaccent_string(regexp_replace(ref.name, '\W', '', 'g')));
		tablename_type:=tablename||'_type';
		field_line_convert:=array_to_string(elementsconvert, ',');
		EXECUTE 'INSERT INTO "'||schemaname||'"."'||tablename||'" SELECT id,(CASE WHEN updated IS NOT NULL THEN updated ELSE created END) as date_row, '||field_line_convert||' FROM data as a, LATERAL(SELECT * FROM json_populate_recordset(null::'||schemaname||'."'||tablename_type||'", (SELECT json_agg(value) FROM data WHERE data.id = a.id AND data.deleted=0))) AS popo WHERE a.id  = '||NEW.id||'; ';
        END IF;

        RETURN NULL;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

UPDATE public.form
   SET
  updated=now();
