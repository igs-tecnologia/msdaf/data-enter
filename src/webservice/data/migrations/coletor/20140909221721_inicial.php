<?php

use Phinx\Migration\AbstractMigration;

class Inicial extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("CREATE EXTENSION IF NOT EXISTS postgis;");
        $this->createGroup();
        $this->createForm();
        $this->createData();
        $this->createFile();
        
        $this->createTriggers();
    }
    
    protected function createGroup()
    {
        $table = $this->table('group');
        $table->addColumn('name', 'string', ['limit' => 255])
              ->addColumn('deleted', 'integer', ['default' => 0])
              ->addColumn('user_id', 'integer',['null'=>true])
              ->addIndex(['user_id'])
              ->save();

        $this->execute("INSERT INTO public.group(name,deleted,user_id) VALUES ('Raiz',0,1);");
    }
    
    protected function createForm()
    {
        $table = $this->table('form');
        $table->addColumn('name', 'string', ['limit' => 255])
              ->addColumn('created', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
              ->addColumn('deleted', 'integer', ['default' => 0])
              ->addColumn('group_id', 'integer')
              ->addColumn('user_id', 'integer',['null'=>true])
              ->addColumn('layout', 'string', ['limit' => 255])
              ->addColumn('struct', 'json')
              ->addColumn('updated', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
              ->addIndex(['group_id'])
              ->addIndex(['user_id'])
              ->addForeignKey('group_id', '"group"')
              ->save();
    }
    
    protected function createData()
    {
        $table = $this->table('data');
        $table->addColumn('created', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
              ->addColumn('deleted', 'integer', ['default' => 0])
              ->addColumn('form_id', 'integer')
              ->addColumn('user_id', 'integer',['null'=>true])
              ->addColumn('plain', 'text')
              ->addColumn('value', 'json')
              ->addColumn('updated', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
              ->addIndex(['form_id'])
              ->addIndex(['user_id'])
              ->addForeignKey('form_id', 'form')
              ->save();
    }
    
    protected function createFile()
    {
        $table = $this->table('file');
        $table->addColumn('name', 'string', ['limit' => 255])
              ->addColumn('created', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
              ->addColumn('deleted', 'integer', ['default' => 0])
              ->addColumn('form_id', 'integer')
              ->addColumn('user_id', 'integer',['null'=>true])
              ->addColumn('detail', 'text')
              ->addColumn('status', 'string', ['limit' => 20])
              ->addColumn('updated', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
              ->addIndex(['form_id'])
              ->addIndex(['user_id'])
              ->addForeignKey('form_id', 'form')
              ->save();
    }
    
    protected function createTriggers()
    {
        $this->execute(file_get_contents(__DIR__ . '/triggers/create_table.sql'));
        $this->execute(file_get_contents(__DIR__ . '/triggers/insert_data.sql'));
    }
}