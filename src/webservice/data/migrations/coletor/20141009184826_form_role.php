<?php

use Phinx\Migration\AbstractMigration;

class FormRole extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        $table = $this->table('form_role', ['id' => false, 'primary_key' => ['form_id', 'role_id']]);
        $table->addColumn('form_id', 'integer')
              ->addColumn('role_id', 'integer')
              ->addIndex(['form_id', 'role_id'], array('unique' => true))
              ->addForeignKey('form_id', '"form"')
              ->save();  
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }
}