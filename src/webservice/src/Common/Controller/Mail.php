<?php

namespace Webservice\Common\Controller;

use Phalcon\Mvc\Controller;
use Webservice\Common\Service\Smtp;
use Webservice\Mvc\Response;

class Mail extends Controller 
{
    public function send()
    {
        $to      = $this->request->get('to');
        $subject = $this->request->get('subject');
        $body    = $this->request->get('body');
        
        $smtp = new Smtp();
        $smtp->send($to, $subject, $body, $this->config->mail);        
        return Response::success([
            'message' => 'Email enviado com sucesso!'
        ]);
    }
   
}