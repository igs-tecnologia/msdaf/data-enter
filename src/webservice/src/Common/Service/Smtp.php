<?php

namespace Webservice\Common\Service;

use Webservice\Service\ServiceAbstract;
use Swift_Message as Message;
use Swift_SmtpTransport as Transport;
use Swift_Mailer as Mailer;

class Smtp extends ServiceAbstract 
{
    protected $transport;

    public function send($to, $subject, $body, $mailSettings) 
    {
        $message = Message::newInstance()
                ->setSubject($subject)
                ->setTo($to)
                ->setFrom([
                    $mailSettings->fromEmail => $mailSettings->fromName
                ])
                ->setBody($body, 'text/html');

        if (!$this->transport) {
            $this->transport = Transport::newInstance(
                            $mailSettings->smtp->server, 
                            $mailSettings->smtp->port, 
                            $mailSettings->smtp->security
                    )
                    ->setUsername($mailSettings->smtp->username)
                    ->setPassword($mailSettings->smtp->password);
        }

        $mailer = Mailer::newInstance($this->transport);

        return $mailer->send($message);
    }
}