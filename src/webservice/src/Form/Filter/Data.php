<?php

namespace Webservice\Form\Filter;

use Zend\Validator\Date;
use Zend\Validator\AbstractValidator;
use Zend\Filter\StaticFilter;

class Data
{
    protected $struct;
    protected $filters = [];
    protected $validators = [];
    protected $messages = [];
    protected $defaultData = [];
    protected $data = [];
    protected $format = 'd/m/Y';
    protected $formatTime = 'd/m/Y H:i:s';
    protected $locale = 'pt_BR';
    protected $isValid = true;
    protected $translator;
    
    public function __construct(array $struct)
    {
        $this->struct = $struct;
    }
    
    public function setFormat($dateFormat)
    {
        $this->format = $dateFormat;
    }
    
    public function setLocale($locale)
    {
        $this->locale = $locale;
    }
    
    public function setTranslator($translator)
    {
        AbstractValidator::setDefaultTranslator($translator);
        $this->translator = $translator;
    }
    
    protected function translate($message) 
    {
        if (! $this->translator) {
            return $message;
        }
        
        return $this->translator->translate($message);
    }
    
    public function getData()
    {
        return $this->data;
    }
    
    public function getMessages()
    {
        $messages = [];
        
        foreach ($this->messages as $key => $value) {
            $messages[] = "$key: $value";
        }
        
        return implode(', ', $messages);
    }
    
    public function build()
    {
        foreach ($this->struct as $key => $field) {
            $type      = ucfirst(strtolower($field->nativeType));
            $validator = [$this, 'validate' . $type];
            $filter    = [$this, 'filter' . $type];
            
            if (isset($field->required) && $field->required) {
                $this->validators[$field->nativeName][] = [$this, 'validateRequired'];
            }
            
            if (is_callable($validator)) {
                $this->validators[$field->nativeName][] = function($value, $field) use ($validator) {
                    if (! $value) {
                        return true;
                    }
                    
                    return call_user_func_array($validator, [$value, $field]);
                };
            }
            
            if (is_callable($filter)) {
                $this->filters[$field->nativeName] = ['filter' => FILTER_CALLBACK, 'options' => function($value) use($filter) {
                    if (! $value) {
                        return null;
                    }
                    
                    return call_user_func_array($filter, [$value]);
                }];
            }            
        }
    }
    
    protected function filterInteger($value)
    {
        return str_replace('.','',$value);
    }
    
    protected function filterVarchar($value)
    {
        return filter_var($value, FILTER_SANITIZE_STRING);
    }
    
    protected function filterNumeric($value)
    {
        return (float) StaticFilter::execute($value, 'Zend\I18n\Filter\NumberFormat', ['locale' => $this->locale]);
    }
    
    protected function filterTimestamp($value)
    {
        $date = \DateTime::createFromFormat($this->format, $value);
        $datetime = \DateTime::createFromFormat($this->formatTime, $value);
        
        if ($date) {
            return $date->format('Y-m-d H:i:s');
        }
        
        if ($datetime) {
            return $datetime->format('Y-m-d H:i:s');
        }
        
        if (is_numeric($value)) {
            return date('Y-m-d H:i:s', $value);
        }
        
        return date('Y-m-d 00:00:00');
    }
    
    protected function validateTimestamp($value, $field)
    {
        $date = \DateTime::createFromFormat('Y-m-d H:i:s', $value);
        
        if ($date) {
            if ($date->format($this->format) == $this->defaultData[$field]) {
                return true;
            }
        
            if ($date->format($this->formatTime) == $this->defaultData[$field]) {
                return true;
            }
        
            if ($date->getTimestamp() == $this->defaultData[$field]) {
                return true;
            }
        }
        
        $this->messages[$field] =  $this->translate("The input does not appear to be a valid date");
        return false;
    }
    
    protected function validateNumeric($value, $field)
    {
        $number = round(StaticFilter::execute($value, 'Zend\I18n\Filter\NumberFormat', ['locale' => $this->locale]),2);
        $defaultNumber = round($this->defaultData[$field],2); //accept integers

        if ($number == $defaultNumber) {
            return true;
        }
        
        $this->messages[$field] =  $this->translate("The input does not appear to be a float");
        return false;
    }
    
    protected function validateRequired($value, $field)
    {
        if (trim((string) $value)) {
            return true;
        }
        
        $this->messages[$field] = $this->translate("Value is required and can't be empty");
        return false;
    }
    
    public function validate($data)
    {
        foreach ($this->validators as $field => $validators) {
            foreach ($validators as $validator) {
                if (! call_user_func_array($validator, [$data[$field], $field])) {
                    $this->isValid = false;
                }
            }
        }
        
        return $this->isValid;
    }
    
    public function getFilters()
    {
        return $this->filters;
    }
    
    public function filter($data)
    {
        $this->build();
        
        foreach ($data as $key => $row) {
            if (is_array($row)) {
                $data[$key] = implode(',', $row);
            }
        }
        
        $this->defaultData = $data;
        $this->data = filter_var_array($data, $this->getFilters());
        
        return $this->data;
    }
    
    public function isValid($data)
    {
        return $this->validate($this->filter($data));
    }
}