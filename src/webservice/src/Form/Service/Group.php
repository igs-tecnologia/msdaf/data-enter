<?php

namespace Webservice\Form\Service;

use Webservice\Form\Model\Group as GroupModel;
use Webservice\Service\ServiceAbstract;

class Group extends ServiceAbstract
{
    public function getAll()
    {
        return GroupModel::find([
            'conditions' => 'deleted = 0',
            'columns' => ['id, name'],
            'order' => 'id DESC'
        ]);
    }
    
    public function getById($id, $isForView = false)
    {
        if ($isForView) {
            return GroupModel::find([
                'conditions' => 'id = ?0 AND deleted = 0',
                'bind' => [$id],
                'columns' => ['id, name']
            ])[0];
        }
        
        return GroupModel::findFirst($id);
    }
    
    public function insert($data)
    {
        unset($data['id']);
        $model = new GroupModel;
        
        if (! $model->save($data)) {
            $this->errorMessage = current($model->getMessages());
            return false;
        }
        
        return $model->id;
    }
    
    public function update($id, $data)
    {
        if (! $id) {
            $this->errorMessage = 'O id do grupo deve ser informado.';
            return false;
        }
        
        $model = $this->getById($id);
        
        if (! $model) {
            $this->errorMessage = 'Grupo inválido.';
            return false;
        }
        
        if (! $model->save($data)) {
            $this->errorMessage = current($model->getMessages());
            return false;
        }
        
        return true;
    }
    
    public function delete($id)
    {
        $model = $this->getById($id);
        
        if (! $model) {
            $this->errorMessage = 'Grupo inválido.';
            return false;
        }
        
        $model->deleted = 1;
        
        if (! $model->save()) {
            $this->errorMessage = current($model->getMessages());
            return false;
        }
        
        return true;
    }
}