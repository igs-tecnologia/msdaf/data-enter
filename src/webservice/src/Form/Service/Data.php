<?php

namespace Webservice\Form\Service;

use Webservice\Form\Model\Data as DataModel;
use Webservice\Form\Service\Form;
use Webservice\Form\Filter\Data as DataFilter;
use Webservice\Service\ServiceAbstract;

class Data extends ServiceAbstract
{
    protected $total;

    private function unaccent($string)
    {
        if (strpos($string = htmlentities($string, ENT_QUOTES, 'UTF-8'), '&') !== false)
        {
            $string = html_entity_decode(preg_replace('~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|tilde|uml);~i', '$1', $string), ENT_QUOTES, 'UTF-8');
        }

        return $string;
    }

    public function getTotal()
    {
        return $this->total;
    }

    public function getAllDataTable($formId = null,$from = null, $columns = null, $join = null, $where = null, $groupBy = null, $having = null,$orderBy = null,$offset,$limit,$filters = array())
    {
        $db        = $this->getDI()->get('dbData');
        $bind      = [];
        $DataModel= new DataModel();
        $DataModel->setSchemaData('"data_store"');

        $form   = new Form;
        $dataForm=$form->getById($formId);
        $tableName=$dataForm->group_id.'_'.strtolower(preg_replace('/[^a-z0-9]/i', '', $this->unaccent($dataForm->name)));
        $phqlBuilder=$DataModel->getModelsManager()->createBuilder();
        if($from){
            $tableName=$from;
        }
        foreach ( $db->describeColumns(str_replace('"','',$tableName),'data_store') as $col){
            if(isset($filters[$col->getName()])){
                $whereOr=[];
                foreach($filters[$col->getName()] as $fWr){
                    $explodefWr =  explode(':',$fWr);
                    $whereOr[]= '\''.$explodefWr[0].'\'';
                }
                $where[]='"data_store".'.$tableName.'."'.$col->getName().'" IN ('.implode(",",$whereOr).')';
            }
        }
        if($join){
            foreach ($join as $f => &$fr) {
                $fr[0]='data_store."'.$fr[0].'"';
                $phqlBuilder->join($fr[0],$fr[1],$fr[2]);
            }
        }

        $phqlBuilder->from('"data_store".'.$tableName);

        $columns=$columns?$phqlBuilder->columns($columns):null;

        if($where) {
            foreach ($where as $i => $wh) {
                if ($i == 0) {
                    if(is_array($wh)){

                    }else{
                        $phqlBuilder->where($wh);
                    }
                } else {
                    $phqlBuilder->andWhere($wh);
                }
            }
        }
        $groupBy=$groupBy?$phqlBuilder->groupBy($groupBy):null;
        $having=$having?$phqlBuilder->having($having):null;
        $orderBy=$orderBy?$phqlBuilder->orderBy($orderBy):null;
        $phqlTotal=$phqlBuilder->getPhql();
        $phqlBuilder->limit($limit,$offset);
        $phql=$phqlBuilder->getPhql();

        $queryS = $phql;
        $queryS = str_replace(array("[", "]"), "", $queryS);
        $queryS = str_replace(":,", ",", $queryS);
        $queryS = str_replace(":)", ")", $queryS);

        $querySTotal = $phqlTotal;
        $querySTotal = str_replace(array("[", "]"), "", $querySTotal);
        $querySTotal = str_replace(":,", ",", $querySTotal);
        $querySTotal = str_replace(":)", ")", $querySTotal);

        $this->total = $db->fetchAll('SELECT COUNT(0) FROM ('.$querySTotal.') as count ', \Phalcon\Db::FETCH_ASSOC, $bind)[0]['count'];

        return $db->fetchAll($queryS, \Phalcon\Db::FETCH_ASSOC, $bind);
    }

    public function getAll($formId, $query = '', $filters = [], $order, $limit, $offset,$joinReplace)
    {
        $aliasSequece='a';
        $form   = new Form;
        $config = $this->getDI()->get('config');
        $struct = json_decode($form->getById($formId)->struct);

        $join = [];
        $transform='a.value';
        $plainJoin=[];
        $defaultOrder=[];
        $fieldsJoin=[];
        if($joinReplace){
            foreach($struct as $field){
                if($field->type=='options' && $field->adapter=='form'){
                    $aliasSequece++;
                    $explodeField=explode('.',$field->field);

                    $transform =" json_object_update_key($transform, '".$field->nativeName."', (CASE WHEN $aliasSequece.value IS NOT NULL THEN $aliasSequece.value->>'".$explodeField[1]."' ELSE '' END))::json";
                    $join[]="LEFT JOIN data as $aliasSequece ON ($aliasSequece.form_id=".$explodeField[0]." AND a.value->>'".$field->nativeName."'=$aliasSequece.id::text)";
                    $plain[]="LEFT JOIN data as $aliasSequece ON ($aliasSequece.form_id=".$explodeField[0]." AND a.value->>'".$field->nativeName."'=$aliasSequece.id::text)";
                    $plainJoin[]=$aliasSequece.'.plain ILIKE :query';
                    $defaultOrder[]="subquery.value->>'".$field->nativeName."' ASC ";
                    $fieldsJoin[$field->nativeName]=array();
                    $fieldsJoin[$field->nativeName]['alias']=$aliasSequece;
                    $fieldsJoin[$field->nativeName]['field']=$explodeField[1];
                    $fieldsJoin[$field->nativeName]['nativeType']= 'text';
                } else{
                    $fieldsJoin[$field->nativeName]=array();
                    $fieldsJoin[$field->nativeName]['alias']='a';
                    $fieldsJoin[$field->nativeName]['field']=$field->nativeName;
                    $fieldsJoin[$field->nativeName]['nativeType']=$field->type;
                }
            }
        }
        $conditionSubquery = [];
        $condition = [];
        $bind      = [];
        $db        = $this->getDI()->get('dbData');
        if (count($filters)) {
            foreach ($filters as $field => $value) {
                if (is_array($value)) {
                    $mult = [];
                    foreach ($value as $i => $item) {
                        $mult[]  = "subquery.value->>'$field' = :$field$i";
                        $explodeValue = explode(':',$item);
                        $bind[$field.$i] = count($explodeValue)>1?$explodeValue[1]:$item;
                    }
                    $conditionSubquery[] = '(' . implode(' OR ', $mult) . ')';
                } else {
                    $conditionSubquery[]  = "a.value->>'$field' = :$field";
                    $bind[$field] = $value;
                }
            }
        }

        if ($query) {
            $condition[]   = count($plainJoin)!=0?'(a.plain ILIKE :query OR '.implode(' OR ',$plainJoin).')':'(a.plain ILIKE :query'.')';
            $bind['query'] = "%$query%";
        }
        $condition[]     = 'a.form_id = :form_id AND a.deleted = 0';
        $bind['form_id'] = $formId;
        $conditions = implode(' AND ', $condition);

        $conditionsSubquery = implode(' AND ', $conditionSubquery);

        $orderBy='';
        if(count($defaultOrder)==0){
            if($order){
                $orderPart = explode(' ',preg_replace('/\s+/', ' ',$order));
                $nativeType = null;
                foreach($struct as $field) {
                    if($field->nativeName == $orderPart[0]) {
                        $nativeType =$field->nativeType;
                    }
                }
                $ascDesc=isset($orderPart[1])?$orderPart[1]:'';
                $orderBy    = " ORDER BY CAST(subquery.value->>'$orderPart[0]' as $nativeType) ".$ascDesc."";
            }else{
                $orderBy    = ' ORDER BY subquery.id DESC';
            }
        }else{
            if($order){

                $orderPart = explode(' ',preg_replace('/\s+/', ' ',$order));

                $ascDesc=isset($orderPart[1])?$orderPart[1]:'';

                if(isset($fieldsJoin[$orderPart[0]])){

                    $orderBy    = " ORDER BY CAST(subquery.value->>'".$fieldsJoin[$orderPart[0]]['field']."' as ".$fieldsJoin[$orderPart[0]]['nativeType'].") ".$ascDesc."";
                }
            }else{
                $orderBy    = ' ORDER BY '.implode(',',$defaultOrder);
            }

        }
        #$orderBy    = $order ? "a.value->>'$order' " : implode(',',$defaultOrder);
        //Get total of registers

        $sql  = 'SELECT subquery.* FROM (';
        $sql .= 'SELECT a.id, a.form_id, a.created, a.updated, '.$transform.' as value ';
        $sql .= ' FROM data as a ';
        $sql .= ' '.implode(' ',$join).' ';
        $sql .= ' WHERE '.$conditions.' ';
        $sql .= ' ) as subquery ';
        if(count($conditionSubquery)!=0){
            $sql .= ' WHERE '.$conditionsSubquery.' ';
        }
        $sqlCount = 'SELECT COUNT(0) FROM ('.$sql.') as tmp';

        $this->total = $db->fetchAll($sqlCount, \Phalcon\Db::FETCH_ASSOC, $bind)[0]['count'];

        $sql .= ' ' . $orderBy .' LIMIT ' . (int) $limit . ' OFFSET ' . (int) $offset;

        return $db->fetchAll($sql, \Phalcon\Db::FETCH_ASSOC, $bind);
    }
    
    public function getById($id, $isForView = false)
    {
        if ($isForView) {
            return DataModel::find([
                'conditions' => 'id = ?0 AND deleted = 0',
                'bind' => [$id],
                'columns' => ['id, created, updated, value']
            ])[0];
        }
        
        return DataModel::findFirst($id);
    }
    
    public function insert($formId, $data,$userId=null)
    {
        $filter = $this->getFilter($formId);

        if (! $filter->isValid($data)) {
            $this->errorMessage = $filter->getMessages();
            return false;
        }
        
        unset($data['id']);
        $model = new DataModel;
        
        $row = [
            'form_id' => $formId,
            'user_id' => $userId,
            'value'   => json_encode($filter->getData()),
            'plain'   => implode(' ', $filter->getData())
        ];

        if (! $model->save($row)) {
            $this->errorMessage = current($model->getMessages());
            return false;
        }
        
        return $model->id;
    }
    
    public function update($id, $formId, $data,$userId=null)
    {
        if (! $id) {
            $this->errorMessage = 'O id do registro deve ser informado.';
            return false;
        }
        
        $model = $this->getById($id);
        
        if (! $model) {
            $this->errorMessage = 'Registro inválido.';
            return false;
        }
        
        $filter = $this->getFilter($formId);

        if (! $filter->isValid($data)) {
            $this->errorMessage = $filter->getMessages();
            return false;
        }
        
        $row = [
            'form_id' => $formId,
            'user_id' => $userId,
            'value'   => json_encode($filter->getData()),
            'plain'   => implode(' ', $filter->getData())
        ];

        if (! $model->save($row)) {
            $this->errorMessage = current($model->getMessages());
            return false;
        }
        
        return true;
    }
    
    public function delete($id)
    {
        $model = $this->getById($id);
        
        if (! $model) {
            $this->errorMessage = 'Registro inválido.';
            return false;
        }
        
        $model->deleted = 1;
        
        if (! $model->save()) {
            $this->errorMessage = current($model->getMessages());
            return false;
        }
        
        return true;
    }
    
    protected function getFilter($formId)
    {
        $form   = new Form;
        $config = $this->getDI()->get('config');
        $struct = json_decode($form->getById($formId)->struct);
        $filter = new DataFilter($struct);

        $filter->setTranslator($this->getDI()->get('translator'));
        $filter->setFormat($config->locale->format);
        $filter->setLocale($config->locale->locale);
        
        return $filter;
    }
}