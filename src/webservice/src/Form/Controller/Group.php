<?php

namespace Webservice\Form\Controller;

use Webservice\Mvc\Response;
use Webservice\Form\Service\Group as GroupService;
use Webservice\Common\Service\User as UserService;
use Webservice\Form\Service\Form as FormService;
use Phalcon\Mvc\Controller;

class Group extends Controller
{
    public function onConstruct()
    {
        $this->service = new GroupService;
    }
    
    public function index()
    {
        $data = $this->service->getAll();
        
        if (count($data)) {
            $service = new FormService;
            $groups  = $data->toArray();
            
            foreach ($groups as $key => $value) {
                $groups[$key]['forms'] = $service->countByGroup($value['id']);
            }
            
            return Response::success(['group' => $groups]);
        }
        
        return Response::success(['group' => []]);
    }
    
    public function insert()
    {
        $data    = $this->request->getPost();
//        $token   = $this->request->getHeader('X_AUTH_TOKEN');
//        $service = new UserService;
//       $user    = $service->getByToken($token);
        
//        $data['user_id'] = $user->id;
        
        $id = $this->service->insert($data);
        
        if (! $id) {
            return Response::error($this->service->getErrorMessage());
        }
        
        return Response::success(['group' => ['id' => $id]]);
    }
    
    public function view($id)
    {
        $data = $this->service->getById($id, true);
        
        if (! $data) {
            return Response::error($this->service->getErrorMessage());
        }
        
        return Response::success(['group' => $data->toArray()]);
    }
    
    public function update($id)
    {
        $data = $this->request->getPut();
        
        if (! $this->service->update($id, $data)) {
            return Response::error($this->service->getErrorMessage());
        }
        
        return Response::success(['group' => 'updated']);
    }
    
    public function delete($id)
    {
        if (! $this->service->delete($id)) {
            return Response::error($this->service->getErrorMessage());
        }
        
        return Response::success(['group' => 'deleted']);
    }
}