<?php

namespace Webservice\Form\Controller;

use Webservice\Mvc\Response;
use Webservice\Form\Service\File as FileService;
use Phalcon\Mvc\Controller;

class File extends Controller
{
    protected $service;
    
    public function onConstruct()
    {
        $this->service = new FileService;
    }
    
    public function index($formId)
    {
        $data = $this->service->getAll($formId);
        
        if (count($data)) {
            return Response::success([
                'file' => $data->toArray(), 
                'total' => $this->service->getTotal()
            ]);
        }
        
        return Response::success(['file' => [], 'total' => 0]);
    }
    
    public function import()
    {
        $data = $this->request->getPost();
        $file = $this->request->getUploadedFiles()[0];

        if (! $this->service->isValidFile($file)) {
            return Response::error('Arquivo inválido. Envie somente arquivos .csv');
        }
            
        $path = APPLICATION_PATH . '/data/upload/' . $file->getName();
        
        if (! move_uploaded_file($file->getTempName(), $path)) {
            return Response::error('Falha ao enviar o arquivo.');
        }
        
        $data['name'] = $file->getName();
        $data['status'] = 'Enviado';
        $data['detail'] = 'O arquivo foi recebido';
        
        $id = $this->service->insert($data);
        
        if (! $id) {
            return Response::error($this->service->getErrorMessage());
        }
        
        return Response::success(['file' => ['id' => $id]]);        
    }
}