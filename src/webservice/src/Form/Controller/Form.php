<?php

namespace Webservice\Form\Controller;

use Webservice\Mvc\Response;
use Webservice\Form\Service\Form as FormService;
use Webservice\Form\Model\FormRole;
use Phalcon\Mvc\Controller;

class Form extends Controller
{
    public function onConstruct()
    {
        $this->service = new FormService;
    }
    
    public function index()
    {
        $groupId = (int) $this->request->get('group_id');
        $data    = $this->service->getAll($groupId);
        
        if (! count($data)) {
            return Response::success(['form' => []]);
        }
        
        $forms = $data->toArray();
        
        foreach ($forms as $key => $form) {
            $roles = FormRole::findByFormId($form['id'])->toArray();
        
            $forms[$key]['roles'] = array_map(function($value){
                return $value['role_id'];
            }, $roles);
        }
        
        return Response::success(['form' => $forms]);
    }
    
    public function insert()
    {
        $data = $this->request->getPost();
        $id   = $this->service->insert($data);
        
        if (! $id) {
            return Response::error($this->service->getErrorMessage());
        }
        
        return Response::success(['form' => ['id' => $id]]);
    }
    
    public function view($id)
    {
        $data = $this->service->getById($id, true);
        
        if (! $data) {
            return Response::error($this->service->getErrorMessage());
        }
        
        $form  = $data->toArray();
        $roles = FormRole::findByFormId($form['id'])->toArray();
        
        $form['roles'] = array_map(function($value){
            return $value['role_id'];
        }, $roles);
        
        return Response::success(['form' => $form]);
    }
    
    public function update($id)
    {
        $data = $this->request->getPut();

        if (! $this->service->update($id, $data)) {
            return Response::error($this->service->getErrorMessage());
        }
        
        return Response::success(['form' => 'updated']);
    }
    
    public function delete($id)
    {
        if (! $this->service->delete($id)) {
            return Response::error($this->service->getErrorMessage());
        }
        
        return Response::success(['form' => 'deleted']);
    }
}