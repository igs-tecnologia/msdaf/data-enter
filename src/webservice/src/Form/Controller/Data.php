<?php

namespace Webservice\Form\Controller;

use Webservice\Mvc\Response;
use Webservice\Form\Service\Data as DataService;
use Phalcon\Mvc\Controller;

class Data extends Controller
{
    public function onConstruct()
    {
        $this->service = new DataService;
        $this->service->setDI($this->getDI());
    }
    
    public function index($formId)
    {
        $query   = $this->request->get('query');
        $filters = $this->request->get('filters');
        $limit   = $this->request->get('limit');
        $offset  = $this->request->get('offset');
        $order   = $this->request->get('order');
        $joinReplace    = $this->request->get('join') === 'true'? true: false;
        $data    = $this->service->getAll($formId, $query, $filters, $order, $limit, $offset,$joinReplace);
        
        if (count($data)) {
            return Response::success([
                'data' => $data,
                'total' => $this->service->getTotal()
            ]);
        }
        
        return Response::success(['data' => [], 'total' => 0]);
    }

    public function getdatatable($formId)
    {
        $from   = $this->request->get('from');
        $columns   = $this->request->get('columns');
        $join = $this->request->get('join');
        $where = $this->request->get('where');
        $filters = $this->request->get('filters');
        $groupBy = $this->request->get('groupBy');
        $having = $this->request->get('having');
        $orderBy   = $this->request->get('orderBy');
        $offset  = $this->request->get('offset');
        $limit   = $this->request->get('limit');
        $data    = $this->service->getAllDataTable($formId,$from, $columns, $join, $where, $groupBy, $having,$orderBy,$offset,$limit,$filters);
        if (count($data)) {
            return Response::success([
                'data' => ($data),
                'total' => $this->service->getTotal()
            ]);
        }
        return Response::success(['data' => $data, 'total' => 0]);
    }

    
    public function insert()
    {
        $data = $this->request->getPost();

        if (! (isset($data['value']) && $data['value'])) {
            return Response::error('O campos "value" deve ser informado.');
        }
        
        $values = (array) json_decode($data['value']);

        $id = $this->service->insert($data['form_id'], $values,$data['user_id']);

        if (! $id) {
            return Response::error($this->service->getErrorMessage());
        }
        
        return Response::success(['data' => ['id' => $id]]);
    }
    
    public function view($id)
    {
        $data = $this->service->getById($id, true);
        
        if (! $data) {
            return Response::error($this->service->getErrorMessage());
        }
        
        return Response::success(['data' => $data->toArray()]);
    }
    
    public function update($id)
    {
        $data = $this->request->getPut();
        
        if (! (isset($data['value']) && $data['value'])) {
            return Response::error('O campos "value" deve ser informado.');
        }
        
        $values = (array) json_decode($data['value']);
        
        if (! $this->service->update($id, $data['form_id'], $values,$data['user_id'])) {
            return Response::error($this->service->getErrorMessage());
        }

        return Response::success(['data' => 'updated']);
    }
    
    public function delete($id)
    {
        if (! $this->service->delete($id)) {
            return Response::error($this->service->getErrorMessage());
        }
        
        return Response::success(['data' => 'deleted']);
    }
}