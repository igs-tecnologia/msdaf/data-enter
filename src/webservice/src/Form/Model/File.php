<?php

namespace Webservice\Form\Model;

use Phalcon\Mvc\Model;

class File extends Model 
{
    public function initialize() 
    {
        $this->setConnectionService('dbData');
        
        $this->setSchema("public");
        $this->setSource("file");
        
        $this->belongsTo(
            'form_id', 
            'Webservice\Form\Model\Form', 
            'id', 
            ['alias' => 'form']
        );
        
        $this->skipAttributesOnCreate(['created', 'updated','deleted']);
    }

    public function beforeUpdate()
    {
        $this->updated = date('Y-m-d H:i:s');
    }
}