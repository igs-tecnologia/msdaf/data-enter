<?php

namespace Webservice\Mvc;

use JSend\JSendResponse;

class Response extends JSendResponse
{
    public function issent()
    {
        return false;
    }
    
    public static function success(array $data = null)
    {
        return new self(self::SUCCESS, $data);
    }

    public static function fail(array $data = null)
    {
        return new self(self::FAIL, $data);
    }

    public static function error($errorMessage, $errorCode = null, array $data = null)
    {
        return new self(self::ERROR, $data, $errorMessage, $errorCode);
    }
    
    public static function decode($json, $depth = 512, $options = 0)
    {
        $rawDecode = json_decode($json, true, $depth, $options);

        if ($rawDecode === null) {
            throw new \UnexpectedValueException('JSON is invalid.');
        }

        if ((! is_array($rawDecode)) or (! array_key_exists('status', $rawDecode))) {
            throw new InvalidJSendException(
                'JSend must be an object with a valid status.');
        }

        $status = $rawDecode['status'];
        $data = array_key_exists('data', $rawDecode) ? $rawDecode['data'] : null;
        $errorMessage = array_key_exists('message', $rawDecode) ? $rawDecode['message'] : null;
        $errorCode = array_key_exists('code', $rawDecode) ? $rawDecode['code'] : null;

        if ($status === self::ERROR) {
            if ($errorMessage === null) {
                throw new InvalidJSendException('JSend errors must contain a message.');
            }
        } elseif (! array_key_exists('data', $rawDecode)) {
            throw new InvalidJSendException('JSend must contain data unless it is an error.');
        }

        return new self($status, $data, $errorMessage, $errorCode);

    }
}