<?php

namespace Webservice\Authentication\Model;

use Phalcon\Mvc\Model;
use Phalcon\Db\RawValue;

class LoginReset extends Model {

    public $id;
    public $user_id;
    public $ip;
    public $token;
    public $created;
    public $updated;

    public function initialize() {
        $this->setSchema("public");
        $this->setSource("login_reset");
        
        $this->belongsTo(
            'user_id', 'Webservice\Common\Model\User', 'id',
            ['foreignKey' => true]
        );
    }

    public function beforeValidationOnCreate()
    {
        $this->token   = preg_replace('/[^a-zA-Z0-9]/', '', base64_encode(openssl_random_pseudo_bytes(24)));
        $this->created = new RawValue('default');
    }
    
    public function beforeValidationOnUpdate()
    {
        $this->updated = new RawValue('default');
    }    
}
