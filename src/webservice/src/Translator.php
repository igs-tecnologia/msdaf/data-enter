<?php
    
namespace Webservice;

use Zend\I18n\Translator\Translator as I18nTranslator;
use Zend\Validator\Translator\TranslatorInterface;

class Translator extends I18nTranslator implements TranslatorInterface
{
    
}