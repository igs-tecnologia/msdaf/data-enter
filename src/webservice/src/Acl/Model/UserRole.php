<?php

namespace Webservice\Acl\Model;

use Phalcon\Mvc\Model;

class UserRole extends Model 
{
    public $system_id;
    public $user_id;
    public $role_id;

    public function initialize() 
    {
        $this->setSchema("public");
        $this->setSource("user_role");

        $this->belongsTo(
            'system_id', 
            'Webservice\Acl\Model\System',
            'id', 
            array('alias' => 'system')
        );
        
        $this->belongsTo(
            'user_id', 
            'Webservice\Common\Model\User',
            'id', 
            array('alias' => 'user')
        );
                
        $this->belongsTo(
            'role_id', 
            'Webservice\Acl\Model\Role',
            'id', 
            array('alias' => 'role')
        );
    }
}