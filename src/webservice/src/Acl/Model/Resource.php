<?php

namespace Webservice\Acl\Model;

use Phalcon\Mvc\Model;

class Resource extends Model 
{
    public $id;
    public $method;
    public $route;
    public $controller;
    public $action;

    public function initialize() 
    {
        $this->setSchema("public");
        $this->setSource("resource");
    }
}