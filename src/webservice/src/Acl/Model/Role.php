<?php

namespace Webservice\Acl\Model;

use Phalcon\Mvc\Model;

class Role extends Model 
{
    public $id;
    public $name;

    public function initialize() 
    {
        $this->setSchema("public");
        $this->setSource("role");
        
        $this->hasMany('id', 'Webservice\Acl\Model\UserRole', "role_id");
    }
}