<?php

namespace Webservice\Acl\Controller;

use Phalcon\Mvc\Controller;
use Webservice\Mvc\Response;
use Webservice\Acl\Service\Role as RoleService;

class Role extends Controller 
{
    public function index()
    {
        $roles = (new RoleService)->getAll();
        
        if (! $roles) {
            return Response::error('Nenhum perfil cadastrado.');
        }
        
        return Response::success(['role' => $roles->toArray()]);
    }

    public function byUserAndSystem($userId, $systemId)
    {
        $roles = (new RoleService)->getByUserAndSystem($userId, $systemId);

        if (! $roles) {
            return Response::error('Nenhum perfil cadastrado.');
        }

        return Response::success($roles);
    }
    
    public function last($userId, $systemId)
    {
        $role = (new RoleService)->last($userId, $systemId);
        return Response::success(['role_id' => $role]);
    }
}